/*
 * ffplay.cpp
 *
 *  Created on: Aug 19, 2021
 *      Author: frankiezafe
 */

#include "fast_forward_playback.h"

static const char* wanted_stream_spec[AVMEDIA_TYPE_NB] = {0};

void FastForwardPlayback::push_event( FFPLAY_EVENTS ev ) {
	upload_mutex.lock();
	events.push_back( ev );
	upload_mutex.unlock();
}

int FastForwardPlayback::check_stream_specifier(AVFormatContext *s, AVStream *st, const char *spec)
{
    int ret = avformat_match_stream_specifier(s, st, spec);
    if (ret < 0)
        av_log(s, AV_LOG_ERROR, "Invalid stream specifier: %s.\n", spec);
    return ret;
}

AVDictionary *  FastForwardPlayback::filter_codec_opts(AVDictionary *opts, enum AVCodecID codec_id,
                                AVFormatContext *s, AVStream *st, const AVCodec *codec)
{
    AVDictionary    *ret = NULL;
    AVDictionaryEntry *t = NULL;
    int            flags = s->oformat ? AV_OPT_FLAG_ENCODING_PARAM
                                      : AV_OPT_FLAG_DECODING_PARAM;
    char          prefix = 0;
    const AVClass    *cc = avcodec_get_class();

    if (!codec)
        codec            = s->oformat ? avcodec_find_encoder(codec_id)
                                      : avcodec_find_decoder(codec_id);

    switch (st->codecpar->codec_type) {
    case AVMEDIA_TYPE_VIDEO:
        prefix  = 'v';
        flags  |= AV_OPT_FLAG_VIDEO_PARAM;
        break;
    case AVMEDIA_TYPE_AUDIO:
        prefix  = 'a';
        flags  |= AV_OPT_FLAG_AUDIO_PARAM;
        break;
    case AVMEDIA_TYPE_SUBTITLE:
        prefix  = 's';
        flags  |= AV_OPT_FLAG_SUBTITLE_PARAM;
        break;
    }

    while (t = av_dict_get(opts, "", t, AV_DICT_IGNORE_SUFFIX)) {
        const AVClass *priv_class;
        char *p = strchr(t->key, ':');

        /* check stream specification in opt name */
        if (p)
            switch (check_stream_specifier(s, st, p + 1)) {
				case  1:
					*p = 0;
					break;
				case  0:
					continue;
				default:
					break;
            }

        if (av_opt_find(&cc, t->key, NULL, flags, AV_OPT_SEARCH_FAKE_OBJ) ||
            !codec ||
            ((priv_class = codec->priv_class) &&
             av_opt_find(&priv_class, t->key, NULL, flags,
                         AV_OPT_SEARCH_FAKE_OBJ)))
            av_dict_set(&ret, t->key, t->value, 0);
        else if (t->key[0] == prefix &&
                 av_opt_find(&cc, t->key + 1, NULL, flags,
                             AV_OPT_SEARCH_FAKE_OBJ))
            av_dict_set(&ret, t->key + 1, t->value, 0);

        if (p)
            *p = ':';
    }
    return ret;
}

AVDictionary ** FastForwardPlayback::setup_find_stream_info_opts(AVFormatContext *s, AVDictionary *codec_opts)
{
	int i;
	AVDictionary **opts;

	if (!s->nb_streams) {
		return NULL;
	}

	opts = static_cast<AVDictionary**>(av_mallocz_array( s->nb_streams, sizeof(*opts) ));

	if (!opts) {
		av_log(NULL, AV_LOG_ERROR,
			   "Could not alloc memory for stream options.\n");
		return NULL;
	}

	for (i = 0; i < s->nb_streams; i++) {
		opts[i] = filter_codec_opts(codec_opts, s->streams[i]->codecpar->codec_id, s, s->streams[i], NULL);
	}
	return opts;
}

Image::Format FastForwardPlayback::convert_px_format_godot( AVPixelFormat frmt ) {
	switch( frmt ) {
		case AV_PIX_FMT_RGB8:
			return Image::Format::FORMAT_RGB8;
		case AV_PIX_FMT_RGBA:
			return Image::Format::FORMAT_RGBA8;
		case AV_PIX_FMT_RGB24:
			return Image::Format::FORMAT_RGB8;
		case AV_PIX_FMT_RGB0:
			return Image::Format::FORMAT_RGB8;
		default:
			break;
	}
	return Image::Format::FORMAT_MAX;
}

AVPixelFormat FastForwardPlayback::convert_px_format_av( Image::Format frmt ) {
	switch( frmt ) {
		case Image::Format::FORMAT_RGBA8:
			return AV_PIX_FMT_RGBA;
		case Image::Format::FORMAT_RGB8:
			return AV_PIX_FMT_RGB24;
		default:
			break;
	}
	return AV_PIX_FMT_NONE;
}

void* FastForwardPlayback::grow_array(void *array, int elem_size, int *size, int new_size) {
	if (new_size >= INT_MAX / elem_size) {
		av_log(NULL, AV_LOG_ERROR, "Array too big.\n");
		return 0;
	}
	if (*size < new_size) {
		uint8_t *tmp = static_cast<uint8_t* const >(av_realloc_array(array, new_size, elem_size));
		if (!tmp) {
			av_log(NULL, AV_LOG_ERROR, "Could not alloc buffer.\n");
			return 0;
		}
		memset(tmp + *size * elem_size, 0, (new_size - *size) * elem_size);
		*size = new_size;
		return tmp;
	}
	return array;
}

int FastForwardPlayback::cmp_audio_fmts(enum AVSampleFormat fmt1, int64_t channel_count1,
		enum AVSampleFormat fmt2, int64_t channel_count2) {
	/* If channel count == 1, planar and non-planar formats are the same */
	if (channel_count1 == 1 && channel_count2 == 1)
		return av_get_packed_sample_fmt(fmt1) != av_get_packed_sample_fmt(fmt2);
	else
		return channel_count1 != channel_count2 || fmt1 != fmt2;
}

int64_t FastForwardPlayback::get_valid_channel_layout(int64_t channel_layout, int channels) {
	if (channel_layout
			&& av_get_channel_layout_nb_channels(channel_layout) == channels)
		return channel_layout;
	else
		return 0;
}

int FastForwardPlayback::packet_queue_put_private(PacketQueue *q, AVPacket *pkt) {
	MyAVPacketList pkt1;
	if (q->abort_request)
		return -1;
	if (av_fifo_space(q->pkt_list) < sizeof(pkt1)) {
		if (av_fifo_grow(q->pkt_list, sizeof(pkt1)) < 0)
			return -1;
	}
	pkt1.pkt = pkt;
	pkt1.serial = q->serial;
	av_fifo_generic_write(q->pkt_list, &pkt1, sizeof(pkt1), NULL);
	q->nb_packets++;
	q->size += pkt1.pkt->size + sizeof(pkt1);
	q->duration += pkt1.pkt->duration;
	/* XXX: should duplicate packet data in DV case */
	if ( q->pq_cond->get() == 0 ) {
		q->pq_cond->post();
	}
//    SDL_CondSignal(q->cond);
	return 0;
}

int FastForwardPlayback::packet_queue_put(PacketQueue *q, AVPacket *pkt) {
	AVPacket *pkt1;
	int ret;
	pkt1 = av_packet_alloc();
	if (!pkt1) {
		av_packet_unref(pkt);
		return -1;
	}
	av_packet_move_ref(pkt1, pkt);
	q->pq_mutex->lock();
	ret = packet_queue_put_private(q, pkt1);
	q->pq_mutex->unlock();
	if (ret < 0) {
		av_packet_free(&pkt1);
	}
	return ret;
}

int FastForwardPlayback::packet_queue_put_nullpacket(PacketQueue *q, AVPacket *pkt,
		int stream_index) {
	pkt->stream_index = stream_index;
	return packet_queue_put(q, pkt);
}

/* packet queue handling */
int FastForwardPlayback::packet_queue_init(PacketQueue *q) {
	memset(q, 0, sizeof(PacketQueue));
	q->pkt_list = av_fifo_alloc(sizeof(MyAVPacketList));
	if (!q->pkt_list)
		return AVERROR(ENOMEM);
	q->pq_mutex = memnew(Mutex);
	q->pq_cond = memnew(Semaphore);
	q->abort_request = 1;
	return 0;
}

void FastForwardPlayback::packet_queue_flush(PacketQueue *q) {
	MyAVPacketList pkt1;
	q->pq_mutex->lock();
	while (av_fifo_size(q->pkt_list) >= sizeof(pkt1)) {
		av_fifo_generic_read(q->pkt_list, &pkt1, sizeof(pkt1), NULL);
		av_packet_free(&pkt1.pkt);
	}
	q->nb_packets = 0;
	q->size = 0;
	q->duration = 0;
	q->serial++;
	q->pq_mutex->unlock();
}

void FastForwardPlayback::packet_queue_destroy(PacketQueue *q) {
	packet_queue_flush(q);
	av_fifo_freep(&q->pkt_list);
	memdelete( q->pq_mutex );
	memdelete( q->pq_cond );
	q->pq_mutex = NULL;
	q->pq_cond = NULL;
//    SDL_DestroyMutex(q->mutex);
//    SDL_DestroyCond(q->cond);
}

void FastForwardPlayback::packet_queue_abort(PacketQueue *q) {
	q->pq_mutex->lock();
	q->abort_request = 1;
	if ( q->pq_cond->get() == 0 ) {
		q->pq_mutex->unlock();
		q->pq_cond->post();
	}
	q->pq_mutex->unlock();
}

void FastForwardPlayback::packet_queue_start(PacketQueue *q) {
	q->pq_mutex->lock();
	q->abort_request = 0;
	q->serial++;
	q->pq_mutex->unlock();
}

/* return < 0 if aborted, 0 if no packet and > 0 if packet.  */
int FastForwardPlayback::packet_queue_get(PacketQueue *q, AVPacket *pkt, int block, int *serial) {

	MyAVPacketList pkt1;
	int ret;
	q->pq_mutex->lock();

	for (;;) {

		if (q->abort_request) {
			ret = -1;
			break;
		}
		if (av_fifo_size(q->pkt_list) >= sizeof(pkt1)) {
			av_fifo_generic_read(q->pkt_list, &pkt1, sizeof(pkt1), NULL);
			q->nb_packets--;
			q->size -= pkt1.pkt->size + sizeof(pkt1);
			q->duration -= pkt1.pkt->duration;
			av_packet_move_ref(pkt, pkt1.pkt);
			if (serial)
				*serial = pkt1.serial;
			av_packet_free(&pkt1.pkt);
			ret = 1;
			break;
		} else if (!block) {
			ret = 0;
			break;
		} else {
//            SDL_CondWait(q->cond, q->mutex);
			q->pq_mutex->unlock();
			q->pq_cond->wait();
		}
	}

	q->pq_mutex->unlock();
	return ret;

}

int FastForwardPlayback::decoder_init(Decoder *d, AVCodecContext *avctx, PacketQueue *queue, Semaphore *empty_queue_cond) {
	memset(d, 0, sizeof(Decoder));
	d->pkt = av_packet_alloc();
	if (!d->pkt) {
		return AVERROR(ENOMEM);
	}
	d->avctx = avctx;
	d->queue = queue;
	d->empty_queue_cond = empty_queue_cond;
	d->start_pts = AV_NOPTS_VALUE;
	d->pkt_serial = -1;
	return 0;
}

int FastForwardPlayback::decoder_decode_frame(Decoder *d, AVFrame *frame, AVSubtitle *sub) {

	int ret = AVERROR(EAGAIN);

	for (;;) {
		if (d->queue->serial == d->pkt_serial) {

			do {
				if (d->queue->abort_request)
					return -1;
				switch (d->avctx->codec_type) {
				case AVMEDIA_TYPE_VIDEO:
					ret = avcodec_receive_frame(d->avctx, frame);
					if (ret >= 0) {
						if (decoder_reorder_pts == -1) {
							frame->pts = frame->best_effort_timestamp;
						} else if (!decoder_reorder_pts) {
							frame->pts = frame->pkt_dts;
						}
					}
					break;
				case AVMEDIA_TYPE_AUDIO:
					ret = avcodec_receive_frame(d->avctx, frame);
					if (ret >= 0) {
						AVRational tb = (AVRational ) { 1, frame->sample_rate };
						if (frame->pts != AV_NOPTS_VALUE)
							frame->pts = av_rescale_q(frame->pts,
									d->avctx->pkt_timebase, tb);
						else if (d->next_pts != AV_NOPTS_VALUE)
							frame->pts = av_rescale_q(d->next_pts,
									d->next_pts_tb, tb);
						if (frame->pts != AV_NOPTS_VALUE) {
							d->next_pts = frame->pts + frame->nb_samples;
							d->next_pts_tb = tb;
						}
					}
					break;
				}
				if (ret == AVERROR_EOF) {
					d->finished = d->pkt_serial;
					avcodec_flush_buffers(d->avctx);
					return 0;
				}
				if (ret >= 0)
					return 1;
			} while (ret != AVERROR(EAGAIN));

		}

		do {

			if (d->queue->nb_packets == 0) {
				if ( d->empty_queue_cond->get() == 0 ) {
					d->empty_queue_cond->post();
				}
			}

			if (d->packet_pending) {

				d->packet_pending = 0;

			} else {

				int old_serial = d->pkt_serial;

				if (packet_queue_get(d->queue, d->pkt, 1, &d->pkt_serial) < 0) {
					return -1;
				}

				if (old_serial != d->pkt_serial) {
					avcodec_flush_buffers(d->avctx);
					d->finished = 0;
					d->next_pts = d->start_pts;
					d->next_pts_tb = d->start_pts_tb;
				}
			}

			if (d->queue->serial == d->pkt_serial) {
				break;
			}

			av_packet_unref(d->pkt);

		} while (1);

		if (d->avctx->codec_type == AVMEDIA_TYPE_SUBTITLE) {
			int got_frame = 0;
			ret = avcodec_decode_subtitle2(d->avctx, sub, &got_frame, d->pkt);
			if (ret < 0) {
				ret = AVERROR(EAGAIN);
			} else {
				if (got_frame && !d->pkt->data) {
					d->packet_pending = 1;
				}
				ret = got_frame ?
						0 : (d->pkt->data ? AVERROR(EAGAIN) : AVERROR_EOF);
			}
			av_packet_unref(d->pkt);
		} else {
			if (avcodec_send_packet(d->avctx, d->pkt) == AVERROR(EAGAIN)) {
				av_log(d->avctx, AV_LOG_ERROR,
						"Receive_frame and send_packet both returned EAGAIN, which is an API violation.\n");
				d->packet_pending = 1;
			} else {
				av_packet_unref(d->pkt);
			}
		}
	}
}

void FastForwardPlayback::decoder_destroy(Decoder *d) {
	av_packet_free(&d->pkt);
	avcodec_free_context(&d->avctx);
}

void FastForwardPlayback::frame_queue_unref_item(Frame *vp) {
	av_frame_unref(vp->frame);
	avsubtitle_free(&vp->sub);
}

int FastForwardPlayback::frame_queue_init(FrameQueue *f, PacketQueue *pktq, int max_size,
		int keep_last) {
	int i;
	memset(f, 0, sizeof(FrameQueue));
	f->fq_mutex = memnew(Mutex);
	f->fq_cond = memnew(Semaphore);
//    if (!(f->mutex = SDL_CreateMutex())) {
//        av_log(NULL, AV_LOG_FATAL, "SDL_CreateMutex(): %s\n", SDL_GetError());
//        return AVERROR(ENOMEM);
//    }
//    if (!(f->cond = SDL_CreateCond())) {
//        av_log(NULL, AV_LOG_FATAL, "SDL_CreateCond(): %s\n", SDL_GetError());
//        return AVERROR(ENOMEM);
//    }
	f->pktq = pktq;
	f->max_size = FFMIN(max_size, FRAME_QUEUE_SIZE);
	f->keep_last = !!keep_last;
	for (i = 0; i < f->max_size; i++)
		if (!(f->queue[i].frame = av_frame_alloc()))
			return AVERROR(ENOMEM);
	return 0;
}

void FastForwardPlayback::frame_queue_destory(FrameQueue *f) {
	int i;
	for (i = 0; i < f->max_size; i++) {
		Frame *vp = &f->queue[i];
		frame_queue_unref_item(vp);
		av_frame_free(&vp->frame);
	}
	memdelete( f->fq_mutex );
	memdelete( f->fq_cond );
	f->fq_mutex = NULL;
	f->fq_cond = NULL;
//    SDL_DestroyMutex(f->mutex);
//    SDL_DestroyCond(f->cond);
}

void FastForwardPlayback::frame_queue_signal(FrameQueue *f) {
	f->fq_mutex->lock();
	if ( f->fq_cond->get() == 0 ) {
		f->fq_mutex->unlock();
		f->fq_cond->post();
	}
	f->fq_mutex->unlock();
}

FastForwardPlayback::Frame* FastForwardPlayback::frame_queue_peek(FrameQueue *f) {
	return &f->queue[(f->rindex + f->rindex_shown) % f->max_size];
}

FastForwardPlayback::Frame* FastForwardPlayback::frame_queue_peek_next(FrameQueue *f) {
	return &f->queue[(f->rindex + f->rindex_shown + 1) % f->max_size];
}

FastForwardPlayback::Frame* FastForwardPlayback::frame_queue_peek_last(FrameQueue *f) {
	return &f->queue[f->rindex];
}

FastForwardPlayback::Frame* FastForwardPlayback::frame_queue_peek_writable(FrameQueue *f) {
	/* wait until we have space to put a new frame */
	f->fq_mutex->lock();
	while (f->size >= f->max_size && !f->pktq->abort_request) {
//        SDL_CondWait(f->cond, f->mutex);
		f->fq_mutex->unlock();
		f->fq_cond->wait();
	}
	f->fq_mutex->unlock();
	if (f->pktq->abort_request) {
		return NULL;
	}
	return &f->queue[f->windex];
}

FastForwardPlayback::Frame* FastForwardPlayback::frame_queue_peek_readable(FrameQueue *f) {
	/* wait until we have a readable a new frame */
	f->fq_mutex->lock();
	while (f->size - f->rindex_shown <= 0 && !f->pktq->abort_request) {
//        SDL_CondWait(f->cond, f->mutex);
		f->fq_mutex->unlock();
		f->fq_cond->wait();
	}
	f->fq_mutex->unlock();
	if (f->pktq->abort_request) {
		return NULL;
	}
	return &f->queue[(f->rindex + f->rindex_shown) % f->max_size];
}

void FastForwardPlayback::frame_queue_push(FrameQueue *f) {

	if (++f->windex == f->max_size) {
		f->windex = 0;
	}

	f->fq_mutex->lock();
	f->size++;
	if ( f->fq_cond->get() == 0 ) {
		f->fq_mutex->unlock();
		f->fq_cond->post();
	}
	f->fq_mutex->unlock();

}

void FastForwardPlayback::frame_queue_next(FrameQueue *f) {

	if (f->keep_last && !f->rindex_shown) {
		f->rindex_shown = 1;
		return;
	}

	frame_queue_unref_item(&f->queue[f->rindex]);

	if (++f->rindex == f->max_size) {
		f->rindex = 0;
	}

	f->fq_mutex->lock();
	f->size--;
	if ( f->fq_cond->get() == 0 ) {
		f->fq_mutex->unlock();
		f->fq_cond->post();
	}
	f->fq_mutex->unlock();

}

/* return the number of undisplayed frames in the queue */
int FastForwardPlayback::frame_queue_nb_remaining(FrameQueue *f) {
	return f->size - f->rindex_shown;
}

/* return last shown position */
int64_t FastForwardPlayback::frame_queue_last_pos(FrameQueue *f) {
	Frame *fp = &f->queue[f->rindex];
	if (f->rindex_shown && fp->serial == f->pktq->serial)
		return fp->pos;
	else
		return -1;
}

void FastForwardPlayback::decoder_abort(Decoder *d, FrameQueue *fq) {
	packet_queue_abort(d->queue);
	frame_queue_signal(fq);
	d->decoder_tid->wait_to_finish();
	memdelete( d->decoder_tid );
    d->decoder_tid = NULL;
	packet_queue_flush(d->queue);
}

void FastForwardPlayback::fill_rectangle(int x, int y, int w, int h) {
	std::cout << "fplay::fill_rectangle" << std::endl;
//    SDL_Rect rect;
//    rect.x = x;
//    rect.y = y;
//    rect.w = w;
//    rect.h = h;
//    if (w && h)
//        SDL_RenderFillRect(renderer, &rect);
}

int FastForwardPlayback::realloc_texture() {
	std::cout << "fplay::realloc_texture" << std::endl;
//	SDL_Texture **texture, Uint32 new_format, int new_width, int new_height, SDL_BlendMode blendmode, int init_texture
//    Uint32 format;
//    int access, w, h;
//    if (!*texture || SDL_QueryTexture(*texture, &format, &access, &w, &h) < 0 || new_width != w || new_height != h || new_format != format) {
//        void *pixels;
//        int pitch;
//        if (*texture)
//            SDL_DestroyTexture(*texture);
//        if (!(*texture = SDL_CreateTexture(renderer, new_format, SDL_TEXTUREACCESS_STREAMING, new_width, new_height)))
//            return -1;
//        if (SDL_SetTextureBlendMode(*texture, blendmode) < 0)
//            return -1;
//        if (init_texture) {
//            if (SDL_LockTexture(*texture, NULL, &pixels, &pitch) < 0)
//                return -1;
//            memset(pixels, 0, pitch * new_height);
//            SDL_UnlockTexture(*texture);
//        }
//        av_log(NULL, AV_LOG_VERBOSE, "Created %dx%d texture with %s.\n", new_width, new_height, SDL_GetPixelFormatName(new_format));
//    }
	return 0;
}

void FastForwardPlayback::calculate_display_rect(Rect2 *rect, int scr_xleft, int scr_ytop,
		int scr_width, int scr_height, int pic_width, int pic_height,
		AVRational pic_sar) {
	AVRational aspect_ratio = pic_sar;
	int64_t width, height, x, y;

	if (av_cmp_q(aspect_ratio, av_make_q(0, 1)) <= 0)
		aspect_ratio = av_make_q(1, 1);

	aspect_ratio = av_mul_q(aspect_ratio, av_make_q(pic_width, pic_height));

	/* XXX: we suppose the screen has a 1.0 pixel ratio */
	height = scr_height;
	width = av_rescale(height, aspect_ratio.num, aspect_ratio.den) & ~1;
	if (width > scr_width) {
		width = scr_width;
		height = av_rescale(width, aspect_ratio.den, aspect_ratio.num) & ~1;
	}
	x = (scr_width - width) / 2;
	y = (scr_height - height) / 2;
	rect->position.x = scr_xleft + x;
	rect->position.y = scr_ytop + y;
	rect->size.x = FFMAX((int )width, 1);
	rect->size.y = FFMAX((int )height, 1);
}

void FastForwardPlayback::get_godot_pix_fmt(int format, int *godot_pix_fmt) {

	*godot_pix_fmt = convert_px_format_godot( AVPixelFormat(format) );

//	int i;
//    *sdl_blendmode = SDL_BLENDMODE_NONE;

//    if (format == AV_PIX_FMT_RGB32   ||
//        format == AV_PIX_FMT_RGB32_1 ||
//        format == AV_PIX_FMT_BGR32   ||
//        format == AV_PIX_FMT_BGR32_1)
//        *sdl_blendmode = SDL_BLENDMODE_BLEND;
//	for (i = 0; i < FF_ARRAY_ELEMS(godot_texture_format_map) - 1; i++) {
//		if (format == godot_texture_format_map[i].format) {
//			*godot_pix_fmt = godot_texture_format_map[i].texture_fmt;
//			return;
//		}
//	}

}

int FastForwardPlayback::upload_texture(AVFrame *frame, SwsContext **img_convert_ctx) {

	int ret = 0;
	int godot_pix_fmt;
	get_godot_pix_fmt(frame->format, &godot_pix_fmt);

	bool init_texture = false;
	bool use_conversion = Image::Format(godot_pix_fmt) == Image::Format::FORMAT_MAX;

	if ( !vid_texture.is_valid() ) {
		vid_texture.instance();
	}

	if ( vid_texture->get_width() != frame->width || vid_texture->get_height() != frame->height ) {
		init_texture = true;
	}

	if ( init_texture ) {

		vid_texture->create(frame->width, frame->height, Image::FORMAT_RGBA8, Texture::FLAG_FILTER | Texture::FLAG_VIDEO_SURFACE);
		std::cout << "ffplay::upload_texture, texture allocated: " << frame->width << "x" << frame->height << std::endl;

		int image_buffer_size = av_image_get_buffer_size( AV_PIX_FMT_RGBA, frame->width, frame->height, 1 );
		if ( image_buffer.resize( image_buffer_size ) != OK ) {
			std::cout << "error while resizing image_buffer!" << std::endl;
		}
		if ( transfer_frame != NULL ) {
		    av_freep(&transfer_frame->data[0]);
		    av_frame_free(&transfer_frame);
		}

		if ( use_conversion ) {

			transfer_frame = av_frame_alloc();
			transfer_frame->format = AV_PIX_FMT_RGBA;
			transfer_frame->width = frame->width;
			transfer_frame->height = frame->height;
			ret = av_image_alloc(
					transfer_frame->data,
					transfer_frame->linesize,
					transfer_frame->width,
					transfer_frame->height,
					AVPixelFormat(transfer_frame->format),
					32);
			if (ret < 0) {
				fprintf(stderr, "Could not allocate raw picture buffer\n");
			}

			std::cout <<
				"converting pixel format from " <<
				av_get_pix_fmt_name(AVPixelFormat(frame->format)) <<
				" to " <<
				av_get_pix_fmt_name(AVPixelFormat(AV_PIX_FMT_RGBA)) <<
				std::endl;

		} else {

			std::cout <<
				"using direct pixel format: " <<
				av_get_pix_fmt_name(AVPixelFormat(frame->format)) <<
				std::endl;

		}

	}

	if ( !use_conversion ) {

		uint8_t* pixels = image_buffer.write().ptr();
		uint8_t** fdata = frame->data;
		int lines = frame->linesize[0];
		for (int y = 0; y < frame->height; y++) {
			memcpy(pixels, fdata[0] + y * lines, frame->width * 4);
			pixels += frame->width * 4;
		}
		Ref<Image> img = memnew(Image(frame->width, frame->height, 0, Image::FORMAT_RGBA8, image_buffer));
		vid_texture->set_data(img);

	} else {

		/* This should only happen if we are not using avfilter... */
		*img_convert_ctx = sws_getCachedContext(
				*img_convert_ctx,
				frame->width, frame->height, AVPixelFormat(frame->format),
				frame->width, frame->height, AV_PIX_FMT_RGBA,
				sws_flags, NULL, NULL, NULL);

		if (*img_convert_ctx != NULL) {
			sws_scale(
					*img_convert_ctx, (const uint8_t * const *)frame->data,
					frame->linesize, 0, frame->height,
					transfer_frame->data, transfer_frame->linesize);

			uint8_t* pixels = image_buffer.write().ptr();
			uint8_t** fdata = transfer_frame->data;
			int lines = transfer_frame->linesize[0];
			for (int y = 0; y < transfer_frame->height; y++) {
				memcpy(pixels, fdata[0] + y * lines, transfer_frame->width * 4);
				pixels += transfer_frame->width * 4;
			}
			Ref<Image> img = memnew(Image(frame->width, frame->height, 0, Image::FORMAT_RGBA8, image_buffer));
			// Error load_bmp_from_buffer(const PoolVector<uint8_t> &p_array);
			vid_texture->set_data(img);

		} else {

			av_log(NULL, AV_LOG_FATAL, "Cannot initialize the conversion context\n");
			ret = -1;

		}

	}

	return ret;

}

void FastForwardPlayback::set_godot_yuv_conversion_mode(AVFrame *frame) {
//	std::cout << "fplay::set_godot_yuv_conversion_mode" << std::endl;
//#if SDL_VERSION_ATLEAST(2,0,8)
//    SDL_YUV_CONVERSION_MODE mode = SDL_YUV_CONVERSION_AUTOMATIC;
//    if (frame && (frame->format == AV_PIX_FMT_YUV420P || frame->format == AV_PIX_FMT_YUYV422 || frame->format == AV_PIX_FMT_UYVY422)) {
//        if (frame->color_range == AVCOL_RANGE_JPEG)
//            mode = SDL_YUV_CONVERSION_JPEG;
//        else if (frame->colorspace == AVCOL_SPC_BT709)
//            mode = SDL_YUV_CONVERSION_BT709;
//        else if (frame->colorspace == AVCOL_SPC_BT470BG || frame->colorspace == AVCOL_SPC_SMPTE170M || frame->colorspace == AVCOL_SPC_SMPTE240M)
//            mode = SDL_YUV_CONVERSION_BT601;
//    }
//    SDL_SetYUVConversionMode(mode);
//#endif
}

void FastForwardPlayback::video_image_display() {

	Frame *vp;
	Frame *sp = NULL;
	Rect2 rect;

	vp = frame_queue_peek_last(&vstate->pictq);

	if (vstate->subtitle_st) {

		if (frame_queue_nb_remaining(&vstate->subpq) > 0) {
			sp = frame_queue_peek(&vstate->subpq);

			if (vp->pts
					>= sp->pts + ((float) sp->sub.start_display_time / 1000)) {
				if (!sp->uploaded) {
					uint8_t *pixels[4];
					int pitch[4];
					int i;
					if (!sp->width || !sp->height) {
						sp->width = vp->width;
						sp->height = vp->height;
					}

//                    if (realloc_texture(&vstate->sub_texture, SDL_PIXELFORMAT_ARGB8888, sp->width, sp->height, SDL_BLENDMODE_BLEND, 1) < 0)
//                        return;

					for (i = 0; i < sp->sub.num_rects; i++) {
						AVSubtitleRect *sub_rect = sp->sub.rects[i];

						sub_rect->x = av_clip(sub_rect->x, 0, sp->width);
						sub_rect->y = av_clip(sub_rect->y, 0, sp->height);
						sub_rect->w = av_clip(sub_rect->w, 0,
								sp->width - sub_rect->x);
						sub_rect->h = av_clip(sub_rect->h, 0,
								sp->height - sub_rect->y);

						vstate->sub_convert_ctx = sws_getCachedContext(
								vstate->sub_convert_ctx, sub_rect->w, sub_rect->h,
								AV_PIX_FMT_PAL8, sub_rect->w, sub_rect->h,
								AV_PIX_FMT_BGRA, 0, NULL, NULL, NULL);
						if (!vstate->sub_convert_ctx) {
							av_log(NULL, AV_LOG_FATAL,
									"Cannot initialize the conversion context\n");
							return;
						}
						std::cout << "sws_scale!" << std::endl;
//                        if (!SDL_LockTexture(vstate->sub_texture, (SDL_Rect *)sub_rect, (void **)pixels, pitch)) {
//                            sws_scale(vstate->sub_convert_ctx, (const uint8_t * const *)sub_rect->data, sub_rect->linesize,
//                                      0, sub_rect->h, pixels, pitch);
//                            SDL_UnlockTexture(vstate->sub_texture);
//                        }
					}
					sp->uploaded = 1;
				}
			} else
				sp = NULL;
		}
	}

	calculate_display_rect(&rect, vstate->xleft, vstate->ytop, vstate->width, vstate->height, vp->width, vp->height, vp->sar);

	if (!vp->uploaded) {
		if (upload_texture(vp->frame, &vstate->img_convert_ctx) < 0) {
			return;
		}
		vp->uploaded = 1;
		vp->flip_v = vp->frame->linesize[0] < 0;
	}

//	set_godot_yuv_conversion_mode(vp->frame);
//  SDL_RenderCopyEx(renderer, vstate->vid_texture, NULL, &rect, 0, NULL, vp->flip_v ? SDL_FLIP_VERTICAL : 0);
//	set_godot_yuv_conversion_mode(NULL);

	if (sp) {
		int i;
		double xratio = (double) rect.size.x / (double) sp->width;
		double yratio = (double) rect.size.y / (double) sp->height;
		for (i = 0; i < sp->sub.num_rects; i++) {
//            SDL_Rect *sub_rect = (SDL_Rect*)sp->sub.rects[i];
//            SDL_Rect target = {.x = rect.x + sub_rect->x * xratio,
//                               .y = rect.y + sub_rect->y * yratio,
//                               .w = sub_rect->w * xratio,
//                               .h = sub_rect->h * yratio};
//            SDL_RenderCopy(renderer, vstate->sub_texture, sub_rect, &target);
		}
	}

}

int FastForwardPlayback::compute_mod(int a, int b) {
	return a < 0 ? a % b + b : a % b;
}

void FastForwardPlayback::video_audio_display() {

	int i, i_start, x, y1, y, ys, delay, n, nb_display_channels;
	int ch, channels, h, h2;
	int64_t time_diff;
	int rdft_bits, nb_freq;

	for (rdft_bits = 1; (1 << rdft_bits) < 2 * vstate->height; rdft_bits++)
		;
	nb_freq = 1 << (rdft_bits - 1);

	/* compute display index : center on currently output samples */
	channels = vstate->audio_tgt.channels;
	nb_display_channels = channels;
	if (!vstate->paused) {
		int data_used =
				vstate->show_mode == SHOW_MODE_WAVES ? vstate->width : (2 * nb_freq);
		n = 2 * channels;
		delay = vstate->audio_write_buf_size;
		delay /= n;

		/* to be more precise, we take into account the time spent since
		 the last buffer computation */
		if (audio_callback_time) {
			time_diff = av_gettime_relative() - audio_callback_time;
			delay -= (time_diff * vstate->audio_tgt.freq) / 1000000;
		}

		delay += 2 * data_used;
		if (delay < data_used)
			delay = data_used;

		i_start = x = compute_mod(vstate->sample_array_index - delay * channels,
				SAMPLE_ARRAY_SIZE);
		if (vstate->show_mode == SHOW_MODE_WAVES) {
			h = INT_MIN;
			for (i = 0; i < 1000; i += channels) {
				int idx = (SAMPLE_ARRAY_SIZE + x - i) % SAMPLE_ARRAY_SIZE;
				int a = vstate->sample_array[idx];
				int b =
						vstate->sample_array[(idx + 4 * channels) % SAMPLE_ARRAY_SIZE];
				int c =
						vstate->sample_array[(idx + 5 * channels) % SAMPLE_ARRAY_SIZE];
				int d =
						vstate->sample_array[(idx + 9 * channels) % SAMPLE_ARRAY_SIZE];
				int score = a - d;
				if (h < score && (b ^ c) < 0) {
					h = score;
					i_start = idx;
				}
			}
		}

		vstate->last_i_start = i_start;
	} else {
		i_start = vstate->last_i_start;
	}

	if (vstate->show_mode == SHOW_MODE_WAVES) {
//        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

		/* total height for one channel */
		h = vstate->height / nb_display_channels;
		/* graph height / 2 */
		h2 = (h * 9) / 20;
		for (ch = 0; ch < nb_display_channels; ch++) {
			i = i_start + ch;
			y1 = vstate->ytop + ch * h + (h / 2); /* position of center line */
			for (x = 0; x < vstate->width; x++) {
				y = (vstate->sample_array[i] * h2) >> 15;
				if (y < 0) {
					y = -y;
					ys = y1 - y;
				} else {
					ys = y1;
				}
				fill_rectangle(vstate->xleft + x, ys, 1, y);
				i += channels;
				if (i >= SAMPLE_ARRAY_SIZE)
					i -= SAMPLE_ARRAY_SIZE;
			}
		}

//        SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);

		for (ch = 1; ch < nb_display_channels; ch++) {
			y = vstate->ytop + ch * h;
			fill_rectangle(vstate->xleft, y, vstate->width, 1);
		}

	} else {

//        if (realloc_texture(&s->vis_texture, SDL_PIXELFORMAT_ARGB8888, s->width, s->height, SDL_BLENDMODE_NONE, 1) < 0)
//            return;

		if (vstate->xpos >= vstate->width)
			vstate->xpos = 0;
		nb_display_channels = FFMIN(nb_display_channels, 2);
		if (rdft_bits != vstate->rdft_bits) {
			av_rdft_end(vstate->rdft);
			av_free(vstate->rdft_data);
			vstate->rdft = av_rdft_init(rdft_bits, DFT_R2C);
			vstate->rdft_bits = rdft_bits;
			vstate->rdft_data = static_cast<FFTSample*>(av_malloc_array(nb_freq,
					4 * sizeof(*vstate->rdft_data)));
		}
		if (!vstate->rdft || !vstate->rdft_data) {
			av_log(NULL, AV_LOG_ERROR,
					"Failed to allocate buffers for RDFT, switching to waves display\n");
			vstate->show_mode = SHOW_MODE_WAVES;
		} else {
			FFTSample *data[2];
			Rect2 rect = Rect2(vstate->xpos, 0, 1, vstate->height);
			uint32_t *pixels;
			int pitch;
			for (ch = 0; ch < nb_display_channels; ch++) {
				data[ch] = vstate->rdft_data + 2 * nb_freq * ch;
				i = i_start + ch;
				for (x = 0; x < 2 * nb_freq; x++) {
					double w = (x - nb_freq) * (1.0 / nb_freq);
					data[ch][x] = vstate->sample_array[i] * (1.0 - w * w);
					i += channels;
					if (i >= SAMPLE_ARRAY_SIZE)
						i -= SAMPLE_ARRAY_SIZE;
				}
				av_rdft_calc(vstate->rdft, data[ch]);
			}
			/* Least efficient way to do this, we should of course
			 * directly access it but it is more than fast enough. */
//            if (!SDL_LockTexture(s->vis_texture, &rect, (void **)&pixels, &pitch)) {
//                pitch >>= 2;
//                pixels += pitch * s->height;
//                for (y = 0; y < s->height; y++) {
//                    double w = 1 / sqrt(nb_freq);
//                    int a = sqrt(w * sqrt(data[0][2 * y + 0] * data[0][2 * y + 0] + data[0][2 * y + 1] * data[0][2 * y + 1]));
//                    int b = (nb_display_channels == 2 ) ? sqrt(w * hypot(data[1][2 * y + 0], data[1][2 * y + 1]))
//                                                        : a;
//                    a = FFMIN(a, 255);
//                    b = FFMIN(b, 255);
//                    pixels -= pitch;
//                    *pixels = (a << 16) + (b << 8) + ((a+b) >> 1);
//                }
//                SDL_UnlockTexture(s->vis_texture);
//            }
//            SDL_RenderCopy(renderer, s->vis_texture, NULL, NULL);
		}
		if (!vstate->paused)
			vstate->xpos++;
	}
}

void FastForwardPlayback::stream_component_close(int stream_index) {

	AVFormatContext *ic = vstate->ic;
	AVCodecParameters *codecpar;

	if (stream_index < 0 || stream_index >= ic->nb_streams)
		return;
	codecpar = ic->streams[stream_index]->codecpar;

	switch (codecpar->codec_type) {
	case AVMEDIA_TYPE_AUDIO:
		decoder_abort(&vstate->auddec, &vstate->sampq);
//        SDL_CloseAudioDevice(audio_dev);
		decoder_destroy(&vstate->auddec);
		swr_free(&vstate->swr_ctx);
		av_freep(&vstate->audio_buf1);
		vstate->audio_buf1_size = 0;
		vstate->audio_buf = NULL;

		if (vstate->rdft) {
			av_rdft_end(vstate->rdft);
			av_freep(&vstate->rdft_data);
			vstate->rdft = NULL;
			vstate->rdft_bits = 0;
		}
		break;
	case AVMEDIA_TYPE_VIDEO:
		decoder_abort(&vstate->viddec, &vstate->pictq);
		decoder_destroy(&vstate->viddec);
		break;
	case AVMEDIA_TYPE_SUBTITLE:
		decoder_abort(&vstate->subdec, &vstate->subpq);
		decoder_destroy(&vstate->subdec);
		break;
	default:
		break;
	}

	ic->streams[stream_index]->discard = AVDISCARD_ALL;
	switch (codecpar->codec_type) {
	case AVMEDIA_TYPE_AUDIO:
		vstate->audio_st = NULL;
		vstate->audio_stream = -1;
		break;
	case AVMEDIA_TYPE_VIDEO:
		vstate->video_st = NULL;
		vstate->video_stream = -1;
		break;
	case AVMEDIA_TYPE_SUBTITLE:
		vstate->subtitle_st = NULL;
		vstate->subtitle_stream = -1;
		break;
	default:
		break;
	}
}

void FastForwardPlayback::stream_close() {

	/* XXX: use a special url_shutdown call to abort parse cleanly */
	vstate->abort_request = 1;
	vstate->read_tid->wait_to_finish();

	void packetqueue_close( PacketQueue* fq );

	/* close each stream */
	if (vstate->audio_stream >= 0) {
		stream_component_close(vstate->audio_stream);
	}
	if (vstate->video_stream >= 0) {
		stream_component_close(vstate->video_stream);
	}
	if (vstate->subtitle_stream >= 0) {
		stream_component_close(vstate->subtitle_stream);
	}

	avformat_close_input(&vstate->ic);

	packet_queue_destroy(&vstate->videoq);
	packet_queue_destroy(&vstate->audioq);
	packet_queue_destroy(&vstate->subtitleq);

	/* free all pictures */
	frame_queue_destory(&vstate->pictq);
	frame_queue_destory(&vstate->sampq);
	frame_queue_destory(&vstate->subpq);
//	while( vstate->continue_read_thread->get() > 0 ) {
//		vstate->continue_read_thread->post();
//	}
//    SDL_DestroyCond(vstate->continue_read_thread);
	sws_freeContext(vstate->img_convert_ctx);
	sws_freeContext(vstate->sub_convert_ctx);
	av_free(vstate->filename);
	if (vis_texture.is_valid()) {
		vis_texture.unref();
	}
	if (vid_texture.is_valid()) {
		vid_texture.unref();
	}
	if (sub_texture.is_valid()) {
		sub_texture.unref();
	}

	memdelete( vstate->read_tid );
	vstate->read_tid = NULL;
	memdelete( vstate->continue_read_thread );
	vstate->continue_read_thread = NULL;

	av_free(vstate);
	vstate = NULL;

	if ( transfer_frame != NULL ) {
		av_freep(&transfer_frame->data[0]);
		av_frame_free(&transfer_frame);
		transfer_frame = NULL;
	}

}

void FastForwardPlayback::do_quit() {

	if (vstate) {

		stream_close();
		av_dict_free(&swr_opts);
		av_dict_free(&sws_dict);
		av_dict_free(&format_opts);
		av_dict_free(&codec_opts);
		av_dict_free(&resample_opts);

	}

	upload_mutex.lock();
	playback_position = 0;
	upload_mutex.unlock();

}

int FastForwardPlayback::video_open()
{
    int w,h;

    w = screen_width ? screen_width : default_width;
    h = screen_height ? screen_height : default_height;

//    SDL_SetWindowTitle(window, window_title);
//    SDL_SetWindowSize(window, w, h);
//    SDL_SetWindowPosition(window, screen_left, screen_top);
//    if (is_full_screen)
//        SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
//    SDL_ShowWindow(window);

    vstate->width  = w;
    vstate->height = h;

    return 0;
}

/* display the current picture, if any */
void FastForwardPlayback::video_display() {

	if (!vstate->width) {
		video_open();
	}
	if (vstate->audio_st && vstate->show_mode != SHOW_MODE_VIDEO) {
		video_audio_display();
	} else if (vstate->video_st) {
		video_image_display();
	}

}

double FastForwardPlayback::get_clock(Clock *c) {
	if (*c->queue_serial != c->serial)
		return NAN;
	if (c->paused) {
		return c->pts;
	} else {
		double time = av_gettime_relative() / 1000000.0;
		return c->pts_drift + time - (time - c->last_updated) * (1.0 - c->speed);
	}
}

void FastForwardPlayback::set_clock_at(Clock *c, double pts, int serial, double time) {
	c->pts = pts;
	c->last_updated = time;
	c->pts_drift = c->pts - time;
	c->serial = serial;
}

void FastForwardPlayback::set_clock(Clock *c, double pts, int serial) {
	double time = av_gettime_relative() / 1000000.0;
	set_clock_at(c, pts, serial, time);
}

void FastForwardPlayback::set_clock_speed(Clock *c, double speed) {
	set_clock(c, get_clock(c), c->serial);
	c->speed = speed;
}

void FastForwardPlayback::init_clock(Clock *c, int *queue_serial) {
	c->speed = 1.0;
	c->paused = 0;
	c->queue_serial = queue_serial;
	set_clock(c, NAN, -1);
}

void FastForwardPlayback::sync_clock_to_slave(Clock *c, Clock *slave) {
	double clock = get_clock(c);
	double slave_clock = get_clock(slave);
	if (!isnan(slave_clock)
			&& (isnan(clock) || fabs(clock - slave_clock) > AV_NOSYNC_THRESHOLD))
		set_clock(c, slave_clock, slave->serial);
}

int FastForwardPlayback::get_master_sync_type() {
	if (vstate->av_sync_type == AV_SYNC_VIDEO_MASTER) {
		if (vstate->video_st)
			return AV_SYNC_VIDEO_MASTER;
		else
			return AV_SYNC_AUDIO_MASTER;
	} else if (vstate->av_sync_type == AV_SYNC_AUDIO_MASTER) {
		if (vstate->audio_st)
			return AV_SYNC_AUDIO_MASTER;
		else
			return AV_SYNC_EXTERNAL_CLOCK;
	} else {
		return AV_SYNC_EXTERNAL_CLOCK;
	}
}

/* get the current master clock value */
double FastForwardPlayback::get_master_clock() {
	double val;

	switch (get_master_sync_type()) {
	case AV_SYNC_VIDEO_MASTER:
		val = get_clock(&vstate->vidclk);
		break;
	case AV_SYNC_AUDIO_MASTER:
		val = get_clock(&vstate->audclk);
		break;
	default:
		val = get_clock(&vstate->extclk);
		break;
	}
	return val;
}

void FastForwardPlayback::check_external_clock_speed() {
	if (vstate->video_stream
			>= 0&& vstate->videoq.nb_packets <= EXTERNAL_CLOCK_MIN_FRAMES ||
			vstate->audio_stream >= 0 && vstate->audioq.nb_packets <= EXTERNAL_CLOCK_MIN_FRAMES) {
		set_clock_speed(&vstate->extclk,
				FFMAX(EXTERNAL_CLOCK_SPEED_MIN,
						vstate->extclk.speed - EXTERNAL_CLOCK_SPEED_STEP));
	} else if ((vstate->video_stream < 0
			|| vstate->videoq.nb_packets > EXTERNAL_CLOCK_MAX_FRAMES)
			&& (vstate->audio_stream < 0
					|| vstate->audioq.nb_packets > EXTERNAL_CLOCK_MAX_FRAMES)) {
		set_clock_speed(&vstate->extclk,
				FFMIN(EXTERNAL_CLOCK_SPEED_MAX,
						vstate->extclk.speed + EXTERNAL_CLOCK_SPEED_STEP));
	} else {
		double speed = vstate->extclk.speed;
		if (speed != 1.0)
			set_clock_speed(&vstate->extclk,
					speed
							+ EXTERNAL_CLOCK_SPEED_STEP * (1.0 - speed)
									/ fabs(1.0 - speed));
	}
}

/* seek in the stream */
void FastForwardPlayback::stream_seek(int64_t pos, int64_t rel, int seek_by_bytes) {
	if (!vstate->seek_req) {
		vstate->seek_pos = pos;
		vstate->seek_rel = rel;
		vstate->seek_flags &= ~AVSEEK_FLAG_BYTE;
		if (seek_by_bytes)
			vstate->seek_flags |= AVSEEK_FLAG_BYTE;
		vstate->seek_req = 1;
		if ( vstate->continue_read_thread->get() == 0 ) {
			vstate->continue_read_thread->post();
		}
//        SDL_CondSignal(vstate->continue_read_thread);
	}
}

/* pause or resume the video */
void FastForwardPlayback::stream_toggle_pause() {
	if (vstate->paused) {
		vstate->frame_timer += av_gettime_relative() / 1000000.0
				- vstate->vidclk.last_updated;
		if (vstate->read_pause_return != AVERROR(ENOSYS)) {
			vstate->vidclk.paused = 0;
		}
		set_clock(&vstate->vidclk, get_clock(&vstate->vidclk), vstate->vidclk.serial);
	}
	set_clock(&vstate->extclk, get_clock(&vstate->extclk), vstate->extclk.serial);
	vstate->paused = vstate->audclk.paused = vstate->vidclk.paused = vstate->extclk.paused =
			!vstate->paused;
}

void FastForwardPlayback::toggle_pause() {
	stream_toggle_pause();
	vstate->step = 0;
}

void FastForwardPlayback::toggle_mute() {
	vstate->muted = !vstate->muted;
}

void FastForwardPlayback::update_volume(int sign, double step) {
	double volume_level =
			vstate->audio_volume ?
					(20 * log(vstate->audio_volume / (double) MIX_MAXVOLUME)
							/ log(10)) :
					-1000.0;
	int new_volume = lrint(
			MIX_MAXVOLUME * pow(10.0, (volume_level + sign * step) / 20.0));
	vstate->audio_volume = av_clip(
			vstate->audio_volume == new_volume ?
					(vstate->audio_volume + sign) : new_volume, 0,
			MIX_MAXVOLUME);
}

void FastForwardPlayback::step_to_next_frame() {
	/* if the stream is paused unpause it, then step */
	if (vstate->paused)
		stream_toggle_pause();
	vstate->step = 1;
}

double FastForwardPlayback::compute_target_delay(double delay) {
	double sync_threshold, diff = 0;

	/* update delay to follow master synchronisation source */
	if (get_master_sync_type() != AV_SYNC_VIDEO_MASTER) {
		/* if video is slave, we try to correct big delays by
		 duplicating or deleting a frame */
		diff = get_clock(&vstate->vidclk) - get_master_clock();

		/* skip or repeat frame. We take into account the
		 delay to compute the threshold. I still don't know
		 if it is the best guess */
		sync_threshold = FFMAX(AV_SYNC_THRESHOLD_MIN,
				FFMIN(AV_SYNC_THRESHOLD_MAX, delay));
		if (!isnan(diff) && fabs(diff) < vstate->max_frame_duration) {
			if (diff <= -sync_threshold)
				delay = FFMAX(0, delay + diff);
			else if (diff
					>= sync_threshold&& delay > AV_SYNC_FRAMEDUP_THRESHOLD)
				delay = delay + diff;
			else if (diff >= sync_threshold)
				delay = 2 * delay;
		}
	}

	av_log(NULL, AV_LOG_TRACE, "video: delay=%0.3f A-V=%f\n", delay, -diff);

	return delay;
}

double FastForwardPlayback::vp_duration(Frame *vp, Frame *nextvp) {
	if (vp->serial == nextvp->serial) {
		double duration = nextvp->pts - vp->pts;
		if (isnan(duration) || duration <= 0
				|| duration > vstate->max_frame_duration)
			return vp->duration;
		else
			return duration;
	} else {
		return 0.0;
	}
}

void FastForwardPlayback::update_video_pts(double pts, int64_t pos, int serial) {
	/* update current video pts */
	set_clock(&vstate->vidclk, pts, serial);
	sync_clock_to_slave(&vstate->extclk, &vstate->vidclk);
}

/* called to display each frame */
void FastForwardPlayback::video_refresh(double *remaining_time) {

	double time;
	Frame *sp, *sp2;

	if (!vstate->paused && get_master_sync_type() == AV_SYNC_EXTERNAL_CLOCK && vstate->realtime) {
		check_external_clock_speed();
	}

	if (vstate->show_mode != SHOW_MODE_VIDEO && vstate->audio_st) {
		time = av_gettime_relative() / 1000000.0;
		if (vstate->force_refresh || vstate->last_vis_time + rdftspeed < time) {
			video_display();
			vstate->last_vis_time = time;
		}
		*remaining_time = FFMIN(*remaining_time, vstate->last_vis_time + rdftspeed - time);
	}

	if (vstate->video_st) {

		retry: if (frame_queue_nb_remaining(&vstate->pictq) == 0) {

			// nothing to do, no picture to display in the queue

		} else {

			double last_duration, duration, delay;
			Frame *vp, *lastvp;

			/* dequeue the picture */
			lastvp = frame_queue_peek_last(&vstate->pictq);
			vp = frame_queue_peek(&vstate->pictq);

			if (vp->serial != vstate->videoq.serial) {
				frame_queue_next(&vstate->pictq);
				goto retry;
			}

			if (lastvp->serial != vp->serial) {
				vstate->frame_timer = av_gettime_relative() / 1000000.0;
			}

			if (vstate->paused) {
				goto display;
			}

			/* compute nominal last_duration */
			last_duration = vp_duration(lastvp, vp);
			delay = compute_target_delay(last_duration);

			time = av_gettime_relative() / 1000000.0;
			if (time < vstate->frame_timer + delay) { *remaining_time = FFMIN(vstate->frame_timer + delay - time, *remaining_time);
				goto display;
			}

			vstate->frame_timer += delay;
			if (delay > 0 && time - vstate->frame_timer > AV_SYNC_THRESHOLD_MAX)
				vstate->frame_timer = time;


			vstate->pictq.fq_mutex->lock();
			if (!isnan(vp->pts)) {
				update_video_pts(vp->pts, vp->pos, vp->serial);
			}
			vstate->pictq.fq_mutex->unlock();

			if (frame_queue_nb_remaining(&vstate->pictq) > 1) {
				Frame *nextvp = frame_queue_peek_next(&vstate->pictq);
				duration = vp_duration(vp, nextvp);
				if (!vstate->step
						&& (framedrop > 0
								|| (framedrop
										&& get_master_sync_type()
												!= AV_SYNC_VIDEO_MASTER))
						&& time > vstate->frame_timer + duration) {
					vstate->frame_drops_late++;
					frame_queue_next(&vstate->pictq);
					goto retry;
				}
			}

			if (vstate->subtitle_st) {
				while (frame_queue_nb_remaining(&vstate->subpq) > 0) {
					sp = frame_queue_peek(&vstate->subpq);

					if (frame_queue_nb_remaining(&vstate->subpq) > 1)
						sp2 = frame_queue_peek_next(&vstate->subpq);
					else
						sp2 = NULL;

					if (sp->serial != vstate->subtitleq.serial
							|| (vstate->vidclk.pts
									> (sp->pts
											+ ((float) sp->sub.end_display_time
													/ 1000)))
							|| (sp2
									&& vstate->vidclk.pts
											> (sp2->pts
													+ ((float) sp2->sub.start_display_time
															/ 1000)))) {
						if (sp->uploaded) {
							int i;
							for (i = 0; i < sp->sub.num_rects; i++) {
								AVSubtitleRect *sub_rect = sp->sub.rects[i];
								uint8_t *pixels;
								int pitch, j;
								std::cout << "ffplay::video_refresh > push subtitle_st" << std::endl;
							}
						}
						frame_queue_next(&vstate->subpq);
					} else {
						break;
					}
				}
			}

			frame_queue_next(&vstate->pictq);
			vstate->force_refresh = 1;

			if (vstate->step && !vstate->paused) {
				stream_toggle_pause();
			}

		}

		display:

		/* display picture */
		if (vstate->force_refresh && vstate->show_mode == SHOW_MODE_VIDEO && vstate->pictq.rindex_shown) {
			video_display();
		}

	}

	vstate->force_refresh = 0;
	if (show_status) {
		AVBPrint buf;
		static int64_t last_time;
		int64_t cur_time;
		int aqsize, vqsize, sqsize;
		double av_diff;

		cur_time = av_gettime_relative();
		if (!last_time || (cur_time - last_time) >= 30000) {
			aqsize = 0;
			vqsize = 0;
			sqsize = 0;
			if (vstate->audio_st)
				aqsize = vstate->audioq.size;
			if (vstate->video_st)
				vqsize = vstate->videoq.size;
			if (vstate->subtitle_st)
				sqsize = vstate->subtitleq.size;
			av_diff = 0;
			if (vstate->audio_st && vstate->video_st)
				av_diff = get_clock(&vstate->audclk) - get_clock(&vstate->vidclk);
			else if (vstate->video_st)
				av_diff = get_master_clock() - get_clock(&vstate->vidclk);
			else if (vstate->audio_st)
				av_diff = get_master_clock() - get_clock(&vstate->audclk);

			av_bprint_init(&buf, 0, AV_BPRINT_SIZE_AUTOMATIC);
//			av_bprintf(&buf,
//					"%7.2f %s:%7.3f fd=%4d aq=%5dKB vq=%5dKB sq=%5dB f=%"PRId64"/%"PRId64"   \r",
//					get_master_clock(),
//					(vstate->audio_st && vstate->video_st) ? "A-V" : (vstate->video_st ? "M-V" : (vstate->audio_st ? "M-A" : "   ")),
//					av_diff,
//					vstate->frame_drops_early + vstate->frame_drops_late,
//					aqsize / 1024,
//					vqsize / 1024,
//					sqsize,
//					vstate->video_st ? vstate->viddec.avctx->pts_correction_num_faulty_dts : 0,
//					vstate->video_st ? vstate->viddec.avctx->pts_correction_num_faulty_pts : 0);

			if (show_status == 1 && AV_LOG_INFO > av_log_get_level())
				fprintf(stderr, "%s", buf.str);
			else
				av_log(NULL, AV_LOG_INFO, "%s", buf.str);

			fflush (stderr);
			av_bprint_finalize(&buf, NULL);

			last_time = cur_time;
		}
	}
}

int FastForwardPlayback::queue_picture(AVFrame *src_frame, double pts, double duration, int64_t pos, int serial) {

	Frame *vp;

	if (!(vp = frame_queue_peek_writable(&vstate->pictq))) {
		return -1;
	}

	vp->sar = src_frame->sample_aspect_ratio;
	vp->uploaded = 0;

	vp->width = src_frame->width;
	vp->height = src_frame->height;
	vp->format = src_frame->format;

	vp->pts = pts;
	vp->duration = duration;
	vp->pos = pos;
	vp->serial = serial;

	av_frame_move_ref(vp->frame, src_frame);
	frame_queue_push(&vstate->pictq);

	return 0;

}

int FastForwardPlayback::get_video_frame(AVFrame *frame) {

	int got_picture;

	if ((got_picture = decoder_decode_frame(&vstate->viddec, frame, NULL)) < 0) {
		return -1;
	}

	if (got_picture) {

		double dpts = NAN;

		if (frame->pts != AV_NOPTS_VALUE) {
			dpts = av_q2d(vstate->video_st->time_base) * frame->pts;
		}

		frame->sample_aspect_ratio = av_guess_sample_aspect_ratio(vstate->ic, vstate->video_st, frame);

		if (framedrop > 0
				|| (framedrop
						&& get_master_sync_type() != AV_SYNC_VIDEO_MASTER)) {
			if (frame->pts != AV_NOPTS_VALUE) {
				double diff = dpts - get_master_clock();
				if (!isnan(diff) && fabs(diff) < AV_NOSYNC_THRESHOLD
						&& diff - vstate->frame_last_filter_delay < 0
						&& vstate->viddec.pkt_serial == vstate->vidclk.serial
						&& vstate->videoq.nb_packets) {
					vstate->frame_drops_early++;
					av_frame_unref(frame);
					got_picture = 0;
				}
			}
		}

	}

	return got_picture;
}

void FastForwardPlayback::_audio_thread( void * arg ) {

	FastForwardPlayback * obj = reinterpret_cast<FastForwardPlayback *>(arg);
	VideoState* vstate = obj->get_videostate();

	AVFrame *frame = av_frame_alloc();
	Frame *af;
//	int last_serial = -1;
//	int64_t dec_channel_layout;
//	int reconfigure;
	int got_frame = 0;
	AVRational tb;
	int ret = 0;

	if (!frame) {
		return;
//		return AVERROR(ENOMEM);
	}

	do {

		if ((got_frame = obj->decoder_decode_frame(&vstate->auddec, frame, NULL)) < 0) {
			goto the_end;
		}

		if (got_frame) {

			tb = (AVRational ) { 1, frame->sample_rate };

			if (!(af = obj->frame_queue_peek_writable(&vstate->sampq))) {
				goto the_end;
			}

			af->pts =
					(frame->pts == AV_NOPTS_VALUE) ?
							NAN : frame->pts * av_q2d(tb);
			af->pos = frame->pkt_pos;
			af->serial = vstate->auddec.pkt_serial;
			af->duration = av_q2d(
					(AVRational ) { frame->nb_samples,
									frame->sample_rate });

			av_frame_move_ref(af->frame, frame);
			obj->frame_queue_push(&vstate->sampq);

		}

	} while (ret >= 0 || ret == AVERROR(EAGAIN) || ret == AVERROR_EOF);
	the_end: av_frame_free(&frame);

//	return ret;

}

//int ffplay::decoder_start(Decoder *d, int (*fn)(void*), const char *thread_name,
//		void *arg) {
//	packet_queue_start(d->queue);
//	d->decoder_tid = SDL_CreateThread(fn, thread_name, arg);
//	if (!d->decoder_tid) {
//		av_log(NULL, AV_LOG_ERROR, "SDL_CreateThread(): %s\n", SDL_GetError());
//		return AVERROR(ENOMEM);
//	}
//	return 0;
//}

void FastForwardPlayback::_video_thread( void * arg ) {

	FastForwardPlayback * obj = reinterpret_cast<FastForwardPlayback *>(arg);
	VideoState* vstate = obj->get_videostate();

	AVFrame *frame = av_frame_alloc();
	double pts;
	double duration;
	int ret;
	AVRational tb = vstate->video_st->time_base;
	AVRational frame_rate = av_guess_frame_rate(vstate->ic, vstate->video_st, NULL);

	if (!frame) {
		return;
//		return AVERROR(ENOMEM);
	}

	int vt_count = 0;

	for (;;) {

		vt_count++;

		ret = obj->get_video_frame(frame);

		if (ret < 0) {
			goto the_end;
		}

		if (!ret) {
			continue;
		}

		duration = ( frame_rate.num && frame_rate.den ? av_q2d((AVRational ) { frame_rate.den, frame_rate.num }) : 0);
		pts = (frame->pts == AV_NOPTS_VALUE) ? NAN : frame->pts * av_q2d(tb);

		ret = obj->queue_picture(frame, pts, duration, frame->pkt_pos, vstate->viddec.pkt_serial);

		av_frame_unref(frame);

		if (ret < 0) {
			goto the_end;
		}

	}

	the_end: av_frame_free(&frame);

//	return 0;

}

void FastForwardPlayback::_subtitle_thread( void * arg ) {

	FastForwardPlayback * obj = reinterpret_cast<FastForwardPlayback *>(arg);
	VideoState* vstate = obj->get_videostate();

	Frame *sp;
	int got_subtitle;
	double pts;

	for (;;) {

		if (!(sp = obj->frame_queue_peek_writable(&vstate->subpq))) {
			return; // 0;
		}

		if ((got_subtitle = obj->decoder_decode_frame(&vstate->subdec, NULL, &sp->sub))
				< 0)
			break;

		pts = 0;

		if (got_subtitle && sp->sub.format == 0) {
			if (sp->sub.pts != AV_NOPTS_VALUE)
				pts = sp->sub.pts / (double) AV_TIME_BASE;
			sp->pts = pts;
			sp->serial = vstate->subdec.pkt_serial;
			sp->width = vstate->subdec.avctx->width;
			sp->height = vstate->subdec.avctx->height;
			sp->uploaded = 0;

			/* now we can update the picture count */
			obj->frame_queue_push(&vstate->subpq);
		} else if (got_subtitle) {
			avsubtitle_free(&sp->sub);
		}
	}

//	return 0;

}

/* copy samples for viewing in editor window */
void FastForwardPlayback::update_sample_display(short *samples,
		int samples_size) {
	int size, len;

	size = samples_size / sizeof(short);
	while (size > 0) {
		len = SAMPLE_ARRAY_SIZE - vstate->sample_array_index;
		if (len > size)
			len = size;
		memcpy(vstate->sample_array + vstate->sample_array_index, samples,
				len * sizeof(short));
		samples += len;
		vstate->sample_array_index += len;
		if (vstate->sample_array_index >= SAMPLE_ARRAY_SIZE)
			vstate->sample_array_index = 0;
		size -= len;
	}
}

/* return the wanted number of samples to get better sync if sync_type is video
 * or external master clock */
int FastForwardPlayback::synchronize_audio(int nb_samples) {
	int wanted_nb_samples = nb_samples;

	/* if not master, then we try to remove or add samples to correct the clock */
	if (get_master_sync_type() != AV_SYNC_AUDIO_MASTER) {
		double diff, avg_diff;
		int min_nb_samples, max_nb_samples;

		diff = get_clock(&vstate->audclk) - get_master_clock();

		if (!isnan(diff) && fabs(diff) < AV_NOSYNC_THRESHOLD) {
			vstate->audio_diff_cum = diff
					+ vstate->audio_diff_avg_coef * vstate->audio_diff_cum;
			if (vstate->audio_diff_avg_count < AUDIO_DIFF_AVG_NB) {
				/* not enough measures to have a correct estimate */
				vstate->audio_diff_avg_count++;
			} else {
				/* estimate the A-V difference */
				avg_diff = vstate->audio_diff_cum * (1.0 - vstate->audio_diff_avg_coef);

				if (fabs(avg_diff) >= vstate->audio_diff_threshold) {
					wanted_nb_samples = nb_samples
							+ (int) (diff * vstate->audio_src.freq);
					min_nb_samples = ((nb_samples
							* (100 - SAMPLE_CORRECTION_PERCENT_MAX) / 100));
					max_nb_samples = ((nb_samples
							* (100 + SAMPLE_CORRECTION_PERCENT_MAX) / 100));
					wanted_nb_samples = av_clip(wanted_nb_samples,
							min_nb_samples, max_nb_samples);
				}
				av_log(NULL, AV_LOG_TRACE,
						"diff=%f adiff=%f sample_diff=%d apts=%0.3f %f\n", diff,
						avg_diff, wanted_nb_samples - nb_samples,
						vstate->audio_clock, vstate->audio_diff_threshold);
			}
		} else {
			/* too big difference : may be initial PTS errors, so
			 reset A-V filter */
			vstate->audio_diff_avg_count = 0;
			vstate->audio_diff_cum = 0;
		}
	}

	return wanted_nb_samples;
}

/**
 * Decode one audio frame and return its uncompressed size.
 *
 * The processed audio frame is decoded, converted if required, and
 * stored in vstate->audio_buf, with size in bytes given by the return
 * value.
 */
int FastForwardPlayback::audio_decode_frame() {
	int data_size, resampled_data_size;
	int64_t dec_channel_layout;
	av_unused double audio_clock0;
	int wanted_nb_samples;
	Frame *af;

	if (vstate->paused)
		return -1;


	do {
#if defined(_WIN32)
        while (frame_queue_nb_remaining(&vstate->sampq) == 0) {
            if ((av_gettime_relative() - audio_callback_time) > 1000000LL * vstate->audio_hw_buf_size / vstate->audio_tgt.bytes_per_sec / 2)
                return -1;
            av_usleep (1000);
        }
#endif
		if (!(af = frame_queue_peek_readable(&vstate->sampq))) {
			return -1;
		}
		frame_queue_next(&vstate->sampq);
	} while (af->serial != vstate->audioq.serial);


	data_size = av_samples_get_buffer_size(NULL, af->frame->channels, af->frame->nb_samples, AVSampleFormat(af->frame->format), 1);

	dec_channel_layout =
			(af->frame->channel_layout
					&& af->frame->channels
							== av_get_channel_layout_nb_channels(
									af->frame->channel_layout)) ?
					af->frame->channel_layout :
					av_get_default_channel_layout(af->frame->channels);
	wanted_nb_samples = synchronize_audio(af->frame->nb_samples);

	if (af->frame->format != vstate->audio_src.fmt
			|| dec_channel_layout != vstate->audio_src.channel_layout
			|| af->frame->sample_rate != vstate->audio_src.freq
			|| (wanted_nb_samples != af->frame->nb_samples && !vstate->swr_ctx)) {
		swr_free(&vstate->swr_ctx);
		vstate->swr_ctx = swr_alloc_set_opts(NULL, vstate->audio_tgt.channel_layout,
				vstate->audio_tgt.fmt, vstate->audio_tgt.freq, dec_channel_layout,
				AVSampleFormat(af->frame->format), af->frame->sample_rate, 0, NULL);
		if (!vstate->swr_ctx || swr_init(vstate->swr_ctx) < 0) {
			av_log(NULL, AV_LOG_ERROR,
					"Cannot create sample rate converter for conversion of %d Hz %s %d channels to %d Hz %s %d channels!\n",
					af->frame->sample_rate,
					av_get_sample_fmt_name(AVSampleFormat(af->frame->format)),
					af->frame->channels, vstate->audio_tgt.freq,
					av_get_sample_fmt_name(vstate->audio_tgt.fmt),
					vstate->audio_tgt.channels);
			swr_free(&vstate->swr_ctx);
			return -1;
		}
		vstate->audio_src.channel_layout = dec_channel_layout;
		vstate->audio_src.channels = af->frame->channels;
		vstate->audio_src.freq = af->frame->sample_rate;
		vstate->audio_src.fmt = AVSampleFormat(af->frame->format);
	}

	if (vstate->swr_ctx) {
		const uint8_t **in = (const uint8_t**) af->frame->extended_data;
		uint8_t **out = &vstate->audio_buf1;
		int out_count = (int64_t) wanted_nb_samples * vstate->audio_tgt.freq
				/ af->frame->sample_rate + 256;
		int out_size = av_samples_get_buffer_size(NULL, vstate->audio_tgt.channels,
				out_count, vstate->audio_tgt.fmt, 0);
		int len2;
		if (out_size < 0) {
			av_log(NULL, AV_LOG_ERROR, "av_samples_get_buffer_size() failed\n");
			return -1;
		}
		if (wanted_nb_samples != af->frame->nb_samples) {
			if (swr_set_compensation(vstate->swr_ctx,
					(wanted_nb_samples - af->frame->nb_samples)
							* vstate->audio_tgt.freq / af->frame->sample_rate,
					wanted_nb_samples * vstate->audio_tgt.freq
							/ af->frame->sample_rate) < 0) {
				av_log(NULL, AV_LOG_ERROR, "swr_set_compensation() failed\n");
				return -1;
			}
		}
		av_fast_malloc(&vstate->audio_buf1, &vstate->audio_buf1_size, out_size);
		if (!vstate->audio_buf1)
			return AVERROR(ENOMEM);
		len2 = swr_convert(vstate->swr_ctx, out, out_count, in,
				af->frame->nb_samples);
		if (len2 < 0) {
			av_log(NULL, AV_LOG_ERROR, "swr_convert() failed\n");
			return -1;
		}
		if (len2 == out_count) {
			av_log(NULL, AV_LOG_WARNING,
					"audio buffer is probably too small\n");
			if (swr_init(vstate->swr_ctx) < 0)
				swr_free(&vstate->swr_ctx);
		}
		vstate->audio_buf = vstate->audio_buf1;
		resampled_data_size = len2 * vstate->audio_tgt.channels
				* av_get_bytes_per_sample(vstate->audio_tgt.fmt);
	} else {
		vstate->audio_buf = af->frame->data[0];
		resampled_data_size = data_size;
	}

	audio_clock0 = vstate->audio_clock;
	/* update the audio clock with the pts */
	if (!isnan(af->pts))
		vstate->audio_clock = af->pts
				+ (double) af->frame->nb_samples / af->frame->sample_rate;
	else
		vstate->audio_clock = NAN;
	vstate->audio_clock_serial = af->serial;
#ifdef DEBUG
    {
        static double last_clock;
        printf("audio: delay=%0.3f clock=%0.3f clock0=%0.3f\n",
               vstate->audio_clock - last_clock,
               vstate->audio_clock, audio_clock0);
        last_clock = vstate->audio_clock;
    }
#endif
	return resampled_data_size;
}

/* prepare a new audio buffer */
void FastForwardPlayback::godot_audio_callback(void *opaque, uint8_t *stream, int len) {
//     = opaque;
//    int audio_size, len1;
//
//    audio_callback_time = av_gettime_relative();
//
//    while (len > 0) {
//        if (vstate->audio_buf_index >= vstate->audio_buf_size) {
//           audio_size = audio_decode_frame(is);
//           if (audio_size < 0) {
//                /* if error, just output silence */
//               vstate->audio_buf = NULL;
//               vstate->audio_buf_size = SDL_AUDIO_MIN_BUFFER_SIZE / vstate->audio_tgt.frame_size * vstate->audio_tgt.frame_size;
//           } else {
//               if (vstate->show_mode != SHOW_MODE_VIDEO)
//                   update_sample_display(is, (int16_t *)vstate->audio_buf, audio_size);
//               vstate->audio_buf_size = audio_size;
//           }
//           vstate->audio_buf_index = 0;
//        }
//        len1 = vstate->audio_buf_size - vstate->audio_buf_index;
//        if (len1 > len)
//            len1 = len;
//        if (!vstate->muted && vstate->audio_buf && vstate->audio_volume == SDL_MIX_MAXVOLUME)
//            memcpy(stream, (uint8_t *)vstate->audio_buf + vstate->audio_buf_index, len1);
//        else {
//            memset(stream, 0, len1);
//            if (!vstate->muted && vstate->audio_buf)
//                SDL_MixAudioFormat(stream, (uint8_t *)vstate->audio_buf + vstate->audio_buf_index, AUDIO_S16SYS, len1, vstate->audio_volume);
//        }
//        len -= len1;
//        stream += len1;
//        vstate->audio_buf_index += len1;
//    }
//    vstate->audio_write_buf_size = vstate->audio_buf_size - vstate->audio_buf_index;
//    /* Let's assume the audio driver that is used by SDL has two periods. */
//    if (!isnan(vstate->audio_clock)) {
//        set_clock_at(&vstate->audclk, vstate->audio_clock - (double)(2 * vstate->audio_hw_buf_size + vstate->audio_write_buf_size) / vstate->audio_tgt.bytes_per_sec, vstate->audio_clock_serial, audio_callback_time / 1000000.0);
//        sync_clock_to_slave(&vstate->extclk, &vstate->audclk);
//    }
}

int FastForwardPlayback::audio_open(int64_t wanted_channel_layout,
		int wanted_nb_channels, int wanted_sample_rate,
		AudioParams *audio_hw_params) {
//    SDL_AudioSpec wanted_spec, spec;
//    const char *env;
//    static const int next_nb_channels[] = {0, 0, 1, 6, 2, 6, 4, 6};
//    static const int next_sample_rates[] = {0, 44100, 48000, 96000, 192000};
//    int next_sample_rate_idx = FF_ARRAY_ELEMS(next_sample_rates) - 1;
//
//    env = SDL_getenv("SDL_AUDIO_CHANNELS");
//    if (env) {
//        wanted_nb_channels = atoi(env);
//        wanted_channel_layout = av_get_default_channel_layout(wanted_nb_channels);
//    }
//    if (!wanted_channel_layout || wanted_nb_channels != av_get_channel_layout_nb_channels(wanted_channel_layout)) {
//        wanted_channel_layout = av_get_default_channel_layout(wanted_nb_channels);
//        wanted_channel_layout &= ~AV_CH_LAYOUT_STEREO_DOWNMIX;
//    }
//    wanted_nb_channels = av_get_channel_layout_nb_channels(wanted_channel_layout);
//    wanted_spec.channels = wanted_nb_channels;
//    wanted_spec.freq = wanted_sample_rate;
//    if (wanted_spec.freq <= 0 || wanted_spec.channels <= 0) {
//        av_log(NULL, AV_LOG_ERROR, "Invalid sample rate or channel count!\n");
//        return -1;
//    }
//    while (next_sample_rate_idx && next_sample_rates[next_sample_rate_idx] >= wanted_spec.freq)
//        next_sample_rate_idx--;
//    wanted_spec.format = AUDIO_S16SYS;
//    wanted_spec.silence = 0;
//    wanted_spec.samples = FFMAX(SDL_AUDIO_MIN_BUFFER_SIZE, 2 << av_log2(wanted_spec.freq / SDL_AUDIO_MAX_CALLBACKS_PER_SEC));
//    wanted_spec.callback = sdl_audio_callback;
//    wanted_spec.userdata = opaque;
//    while (!(audio_dev = SDL_OpenAudioDevice(NULL, 0, &wanted_spec, &spec, SDL_AUDIO_ALLOW_FREQUENCY_CHANGE | SDL_AUDIO_ALLOW_CHANNELS_CHANGE))) {
//        av_log(NULL, AV_LOG_WARNING, "SDL_OpenAudio (%d channels, %d Hz): %s\n",
//               wanted_spec.channels, wanted_spec.freq, SDL_GetError());
//        wanted_spec.channels = next_nb_channels[FFMIN(7, wanted_spec.channels)];
//        if (!wanted_spec.channels) {
//            wanted_spec.freq = next_sample_rates[next_sample_rate_idx--];
//            wanted_spec.channels = wanted_nb_channels;
//            if (!wanted_spec.freq) {
//                av_log(NULL, AV_LOG_ERROR,
//                       "No more combinations to try, audio open failed\n");
//                return -1;
//            }
//        }
//        wanted_channel_layout = av_get_default_channel_layout(wanted_spec.channels);
//    }
//    if (spec.format != AUDIO_S16SYS) {
//        av_log(NULL, AV_LOG_ERROR,
//               "SDL advised audio format %d is not supported!\n", spec.format);
//        return -1;
//    }
//    if (spec.channels != wanted_spec.channels) {
//        wanted_channel_layout = av_get_default_channel_layout(spec.channels);
//        if (!wanted_channel_layout) {
//            av_log(NULL, AV_LOG_ERROR,
//                   "SDL advised channel count %d is not supported!\n", spec.channels);
//            return -1;
//        }
//    }
//
//    audio_hw_params->fmt = AV_SAMPLE_FMT_S16;
//    audio_hw_params->freq = spec.freq;
//    audio_hw_params->channel_layout = wanted_channel_layout;
//    audio_hw_params->channels =  spec.channels;
//    audio_hw_params->frame_size = av_samples_get_buffer_size(NULL, audio_hw_params->channels, 1, audio_hw_params->fmt, 1);
//    audio_hw_params->bytes_per_sec = av_samples_get_buffer_size(NULL, audio_hw_params->channels, audio_hw_params->freq, audio_hw_params->fmt, 1);
//    if (audio_hw_params->bytes_per_sec <= 0 || audio_hw_params->frame_size <= 0) {
//        av_log(NULL, AV_LOG_ERROR, "av_samples_get_buffer_size failed\n");
//        return -1;
//    }
//    return spec.size;
	return 0;
}

/* open a given stream. Return 0 if OK */
int FastForwardPlayback::stream_component_open(int stream_index) {
	AVFormatContext *ic = vstate->ic;
	AVCodecContext *avctx;
	const AVCodec *codec;
	const char *forced_codec_name = NULL;
	AVDictionary *opts = NULL;
	AVDictionaryEntry *t = NULL;
	int sample_rate, nb_channels;
	int64_t channel_layout;
	int ret = 0;
	int stream_lowres = lowres;

	if (stream_index < 0 || stream_index >= ic->nb_streams)
		return -1;

	avctx = avcodec_alloc_context3(NULL);
	if (!avctx)
		return AVERROR(ENOMEM);

	ret = avcodec_parameters_to_context(avctx,
			ic->streams[stream_index]->codecpar);
	if (ret < 0)
		goto fail;
	avctx->pkt_timebase = ic->streams[stream_index]->time_base;

	codec = avcodec_find_decoder(avctx->codec_id);

	switch (avctx->codec_type) {
	case AVMEDIA_TYPE_AUDIO:
		vstate->last_audio_stream = stream_index;
		forced_codec_name = audio_codec_name;
		break;
	case AVMEDIA_TYPE_SUBTITLE:
		vstate->last_subtitle_stream = stream_index;
		forced_codec_name = subtitle_codec_name;
		break;
	case AVMEDIA_TYPE_VIDEO:
		vstate->last_video_stream = stream_index;
		forced_codec_name = video_codec_name;
		break;
	}
	if (forced_codec_name)
		codec = avcodec_find_decoder_by_name(forced_codec_name);
	if (!codec) {
		if (forced_codec_name)
			av_log(NULL, AV_LOG_WARNING,
					"No codec could be found with name '%s'\n",
					forced_codec_name);
		else
			av_log(NULL, AV_LOG_WARNING,
					"No decoder could be found for codec %s\n",
					avcodec_get_name(avctx->codec_id));
		ret = AVERROR(EINVAL);
		goto fail;
	}

	avctx->codec_id = codec->id;
	if (stream_lowres > codec->max_lowres) {
		av_log(avctx, AV_LOG_WARNING,
				"The maximum value for lowres supported by the decoder is %d\n",
				codec->max_lowres);
		stream_lowres = codec->max_lowres;
	}
	avctx->lowres = stream_lowres;

	if (fast)
		avctx->flags2 |= AV_CODEC_FLAG2_FAST;

	opts = filter_codec_opts(codec_opts, avctx->codec_id, ic,
			ic->streams[stream_index], codec);
	if (!av_dict_get(opts, "threads", NULL, 0))
		av_dict_set(&opts, "threads", "auto", 0);
	if (stream_lowres)
		av_dict_set_int(&opts, "lowres", stream_lowres, 0);
	if ((ret = avcodec_open2(avctx, codec, &opts)) < 0) {
		goto fail;
	}
	if ((t = av_dict_get(opts, "", NULL, AV_DICT_IGNORE_SUFFIX))) {
		av_log(NULL, AV_LOG_ERROR, "Option %s not found.\n", t->key);
		ret = AVERROR_OPTION_NOT_FOUND;
		goto fail;
	}

	vstate->eof = 0;
	ic->streams[stream_index]->discard = AVDISCARD_DEFAULT;

	switch (avctx->codec_type) {

	case AVMEDIA_TYPE_AUDIO:
        sample_rate    = avctx->sample_rate;
        nb_channels    = avctx->channels;
        channel_layout = avctx->channel_layout;

		/* prepare audio output */
		if ((ret = audio_open(channel_layout, nb_channels, sample_rate, &vstate->audio_tgt)) < 0)
			goto fail;
		vstate->audio_hw_buf_size = ret;
		vstate->audio_src = vstate->audio_tgt;
		vstate->audio_buf_size = 0;
		vstate->audio_buf_index = 0;

		/* init averaging filter */
		vstate->audio_diff_avg_coef = exp(log(0.01) / AUDIO_DIFF_AVG_NB);
		vstate->audio_diff_avg_count = 0;
		/* since we do not have a precise anough audio FIFO fullness,
		 we correct audio sync only if larger than this threshold */
		vstate->audio_diff_threshold = (double) (vstate->audio_hw_buf_size)
				/ vstate->audio_tgt.bytes_per_sec;

		vstate->audio_stream = stream_index;
		vstate->audio_st = ic->streams[stream_index];

		if ((ret = decoder_init(&vstate->auddec, avctx, &vstate->audioq, vstate->continue_read_thread)) < 0) {
			goto fail;
		}
		if ((vstate->ic->iformat->flags
				& (AVFMT_NOBINSEARCH | AVFMT_NOGENSEARCH | AVFMT_NO_BYTE_SEEK))
				&& !vstate->ic->iformat->read_seek) {
			vstate->auddec.start_pts = vstate->audio_st->start_time;
			vstate->auddec.start_pts_tb = vstate->audio_st->time_base;
		}
		{
			Decoder * d = &vstate->auddec;
			packet_queue_start(d->queue);
			d->decoder_tid = memnew(Thread);
			d->decoder_tid->start( _audio_thread, this );
			if ( !d->decoder_tid->is_started() ) {
				goto out;
			}
		}
//        SDL_PauseAudioDevice(audio_dev, 0);
		break;

	case AVMEDIA_TYPE_VIDEO:
		vstate->video_stream = stream_index;
		vstate->video_st = ic->streams[stream_index];

		if ((ret = decoder_init(&vstate->viddec, avctx, &vstate->videoq, vstate->continue_read_thread)) < 0) {
			goto fail;
		}
		{
			Decoder * d = &vstate->viddec;
			packet_queue_start(d->queue);
			d->decoder_tid = memnew(Thread);
			d->decoder_tid->start( _video_thread, this);
			if ( !d->decoder_tid->is_started() ) {
				goto out;
			}
		}

		vstate->queue_attachments_req = 1;
		break;

	case AVMEDIA_TYPE_SUBTITLE:

		vstate->subtitle_stream = stream_index;
		vstate->subtitle_st = ic->streams[stream_index];

		if ((ret = decoder_init(&vstate->subdec, avctx, &vstate->subtitleq, vstate->continue_read_thread)) < 0) {
			goto fail;
		}
		{
			Decoder * d = &vstate->subdec;
			packet_queue_start(d->queue);
			d->decoder_tid = memnew(Thread);
			d->decoder_tid->start( _subtitle_thread, this);
			if ( !d->decoder_tid->is_started() ) {
				goto out;
			}
		}
		break;

	default:
		break;
	}
	goto out;

	fail: avcodec_free_context(&avctx);
	out: av_dict_free(&opts);

	return ret;
}

int FastForwardPlayback::_decode_interrupt_cb(void *ctx) {
	VideoState* vs = static_cast<VideoState*>(ctx);
	return vs->abort_request;
}

int FastForwardPlayback::stream_has_enough_packets(AVStream *st, int stream_id,
		PacketQueue *queue) {
	return stream_id < 0 || queue->abort_request
			|| (st->disposition & AV_DISPOSITION_ATTACHED_PIC)
			|| queue->nb_packets > MIN_FRAMES
					&& (!queue->duration
							|| av_q2d(st->time_base) * queue->duration > 1.0);
}

int FastForwardPlayback::is_realtime(AVFormatContext *s) {
	if (!strcmp(s->iformat->name, "rtp") || !strcmp(s->iformat->name, "rtsp")
			|| !strcmp(s->iformat->name, "sdp"))
		return 1;

	if (s->pb && (!strncmp(s->url, "rtp:", 4) || !strncmp(s->url, "udp:", 4)))
		return 1;
	return 0;
}

/* this thread gets the stream from the disk or the network */
void FastForwardPlayback::_read_thread( void * arg ) {

	FastForwardPlayback * obj = reinterpret_cast<FastForwardPlayback *>(arg);
	VideoState* vstate = obj->get_videostate();

	int64_t duration_t = obj->get_duration();
	int64_t start_t = obj->get_start_time();
	int& seek_by_bytes = obj->get_seek_by_bytes();
	AVDictionary* format_opts = obj->get_format_opts();

	AVFormatContext *ic = NULL;
	int err, i, ret;
	int st_index[AVMEDIA_TYPE_NB];
	AVPacket *pkt = NULL;
	int64_t stream_start_time;
	int pkt_in_play_range = 0;
	AVDictionaryEntry *t;
	Mutex* wait_mutex = memnew(Mutex);
	int scan_all_pmts_set = 0;
	int64_t pkt_ts;

	int readcount = 0;

	memset(st_index, -1, sizeof(st_index));
	vstate->eof = 0;

	pkt = av_packet_alloc();
	if (!pkt) {
		av_log(NULL, AV_LOG_FATAL, "Could not allocate packet.\n");
		ret = AVERROR(ENOMEM);
		goto fail;
	}
	ic = avformat_alloc_context();
	if (!ic) {
		av_log(NULL, AV_LOG_FATAL, "Could not allocate context.\n");
		ret = AVERROR(ENOMEM);
		goto fail;
	}
	ic->interrupt_callback.callback = _decode_interrupt_cb;
	ic->interrupt_callback.opaque = vstate;
	if (!av_dict_get(format_opts, "scan_all_pmts", NULL, AV_DICT_MATCH_CASE)) {
		av_dict_set(&format_opts, "scan_all_pmts", "1", AV_DICT_DONT_OVERWRITE);
		scan_all_pmts_set = 1;
	}
	err = avformat_open_input(&ic, vstate->filename, vstate->iformat, &format_opts);
	if (err < 0) {
//		print_error(vstate->filename, err);
		ret = -1;
		goto fail;
	}
	if (scan_all_pmts_set)
		av_dict_set(&format_opts, "scan_all_pmts", NULL, AV_DICT_MATCH_CASE);

	if ((t = av_dict_get(format_opts, "", NULL, AV_DICT_IGNORE_SUFFIX))) {
		av_log(NULL, AV_LOG_ERROR, "Option %s not found.\n", t->key);
		ret = AVERROR_OPTION_NOT_FOUND;
		goto fail;
	}
	vstate->ic = ic;

	if (obj->get_genpts())
		ic->flags |= AVFMT_FLAG_GENPTS;

	av_format_inject_global_side_data(ic);

	if (obj->get_find_stream_info()) {

		AVDictionary* codec_opts = obj->get_codec_opts();
		AVDictionary **opts = setup_find_stream_info_opts(ic, codec_opts);
		int orig_nb_streams = ic->nb_streams;

		err = avformat_find_stream_info(ic, opts);

		for (i = 0; i < orig_nb_streams; i++)
			av_dict_free(&opts[i]);
		av_freep(&opts);

		if (err < 0) {
			av_log(NULL, AV_LOG_WARNING,
					"%s: could not find codec parameters\n", vstate->filename);
			ret = -1;
			goto fail;
		}
	}

	if (ic->pb)
		ic->pb->eof_reached = 0; // FIXME hack, ffplay maybe should not use avio_feof() to test for the end

	if (seek_by_bytes < 0) {
		seek_by_bytes = !!(ic->iformat->flags & AVFMT_TS_DISCONT) && strcmp("ogg", ic->iformat->name);
	}

	vstate->max_frame_duration =
			(ic->iformat->flags & AVFMT_TS_DISCONT) ? 10.0 : 3600.0;

	/* if seeking requested, we execute it */
	if (start_t != AV_NOPTS_VALUE) {
		int64_t timestamp;
		timestamp = start_t;
		/* add the stream start time */
		if (ic->start_time != AV_NOPTS_VALUE)
			timestamp += ic->start_time;
		ret = avformat_seek_file(ic, -1, INT64_MIN, timestamp, INT64_MAX, 0);
		if (ret < 0) {
			av_log(NULL, AV_LOG_WARNING,
					"%s: could not seek to position %0.3f\n", vstate->filename,
					(double) timestamp / AV_TIME_BASE);
		}
	}

	vstate->realtime = is_realtime(ic);

	if (obj->get_show_status()) {
		av_dump_format(ic, 0, vstate->filename, 0);
	}

	for (i = 0; i < ic->nb_streams; i++) {
		AVStream *st = ic->streams[i];
		enum AVMediaType type = st->codecpar->codec_type;
		st->discard = AVDISCARD_ALL;
		if (type >= 0 && wanted_stream_spec[type] && st_index[type] == -1)
			if (avformat_match_stream_specifier(ic, st,
					wanted_stream_spec[type]) > 0)
				st_index[type] = i;
	}
	for (i = 0; i < AVMEDIA_TYPE_NB; i++) {
		if (wanted_stream_spec[i] && st_index[i] == -1) {
			av_log(NULL, AV_LOG_ERROR,
					"Stream specifier %s does not match any %s stream\n",
					wanted_stream_spec[i], av_get_media_type_string(AVMediaType(i)));
			st_index[i] = INT_MAX;
		}
	}

	if (!obj->get_video_disable())
		st_index[AVMEDIA_TYPE_VIDEO] = av_find_best_stream(ic,
				AVMEDIA_TYPE_VIDEO, st_index[AVMEDIA_TYPE_VIDEO], -1, NULL, 0);
	if (!obj->get_audio_disable())
		st_index[AVMEDIA_TYPE_AUDIO] = av_find_best_stream(ic,
				AVMEDIA_TYPE_AUDIO, st_index[AVMEDIA_TYPE_AUDIO],
				st_index[AVMEDIA_TYPE_VIDEO],
				NULL, 0);
	if (!obj->get_video_disable() && !obj->get_subtitle_disable())
		st_index[AVMEDIA_TYPE_SUBTITLE] = av_find_best_stream(ic,
				AVMEDIA_TYPE_SUBTITLE, st_index[AVMEDIA_TYPE_SUBTITLE],
				(st_index[AVMEDIA_TYPE_AUDIO] >= 0 ?
						st_index[AVMEDIA_TYPE_AUDIO] :
						st_index[AVMEDIA_TYPE_VIDEO]),
				NULL, 0);

	if (st_index[AVMEDIA_TYPE_VIDEO] >= 0) {
		AVStream *st = ic->streams[st_index[AVMEDIA_TYPE_VIDEO]];
		AVCodecParameters *codecpar = st->codecpar;
		AVRational sar = av_guess_sample_aspect_ratio(ic, st, NULL);
//		if (codecpar->width)
//			set_default_window_size(codecpar->width, codecpar->height, sar);
	}

	/* open the streams */
	if (st_index[AVMEDIA_TYPE_AUDIO] >= 0) {
		obj->stream_component_open(st_index[AVMEDIA_TYPE_AUDIO]);
	}
	ret = -1;
	if (st_index[AVMEDIA_TYPE_VIDEO] >= 0) {
		ret = obj->stream_component_open(st_index[AVMEDIA_TYPE_VIDEO]);
	}
	if (vstate->show_mode == SHOW_MODE_NONE)
		vstate->show_mode = ret >= 0 ? SHOW_MODE_VIDEO : SHOW_MODE_RDFT;

	if (st_index[AVMEDIA_TYPE_SUBTITLE] >= 0) {
		obj->stream_component_open(st_index[AVMEDIA_TYPE_SUBTITLE]);
	}

	if (vstate->video_stream < 0 && vstate->audio_stream < 0) {
		av_log(NULL, AV_LOG_FATAL,
				"Failed to open file '%s' or configure filtergraph\n",
				vstate->filename);
		ret = -1;
		goto fail;
	}

	if (obj->get_infinite_buffer() < 0 && vstate->realtime) {
		obj->set_infinite_buffer( 1 );
	}

	for (;;) {

		if (vstate->abort_request) {
			break;
		}

		if (vstate->paused != vstate->last_paused) {
			vstate->last_paused = vstate->paused;
			if (vstate->paused)
				vstate->read_pause_return = av_read_pause(ic);
			else
				av_read_play(ic);
		}

#if CONFIG_RTSP_DEMUXER || CONFIG_MMSH_PROTOCOL
        if (vstate->paused &&
                (!strcmp(ic->iformat->name, "rtsp") ||
                 (ic->pb && !strncmp(input_filename, "mmsh:", 5)))) {
            /* wait 10 ms to avoid trying to get another packet */
            /* XXX: horrible */
        	OS::get_singleton()->delay_usec(10000);
            continue;
        }
#endif

		if (vstate->seek_req) {
			int64_t seek_target = vstate->seek_pos;
			int64_t seek_min =
					vstate->seek_rel > 0 ?
							seek_target - vstate->seek_rel + 2 : INT64_MIN;
			int64_t seek_max =
					vstate->seek_rel < 0 ?
							seek_target - vstate->seek_rel - 2 : INT64_MAX;
// FIXME the +-2 is due to rounding being not done in the correct direction in generation
//      of the seek_pos/seek_rel variables

			ret = avformat_seek_file(vstate->ic, -1, seek_min, seek_target,
					seek_max, vstate->seek_flags);
			if (ret < 0) {
				av_log(NULL, AV_LOG_ERROR, "%s: error while seeking\n",
						vstate->ic->url);
			} else {
				if (vstate->audio_stream >= 0)
					obj->packet_queue_flush(&vstate->audioq);
				if (vstate->subtitle_stream >= 0)
					obj->packet_queue_flush(&vstate->subtitleq);
				if (vstate->video_stream >= 0)
					obj->packet_queue_flush(&vstate->videoq);
				if (vstate->seek_flags & AVSEEK_FLAG_BYTE) {
					obj->set_clock(&vstate->extclk, NAN, 0);
				} else {
					obj->set_clock(&vstate->extclk, seek_target / (double) AV_TIME_BASE,0);
				}
			}
			vstate->seek_req = 0;
			vstate->queue_attachments_req = 1;
			vstate->eof = 0;
			if (vstate->paused) {
				obj->step_to_next_frame();
			}
		}

		if (vstate->queue_attachments_req) {
			if (vstate->video_st && vstate->video_st->disposition & AV_DISPOSITION_ATTACHED_PIC) {
				if ((ret = av_packet_ref(pkt, &vstate->video_st->attached_pic)) < 0) {
					goto fail;
				}
				obj->packet_queue_put(&vstate->videoq, pkt);
				obj->packet_queue_put_nullpacket(&vstate->videoq, pkt, vstate->video_stream);
			}
			vstate->queue_attachments_req = 0;
		}

		/* if the queue are full, no need to read more */
		if (obj->get_infinite_buffer() < 1
				&& (vstate->audioq.size + vstate->videoq.size + vstate->subtitleq.size
						> MAX_QUEUE_SIZE
						|| (obj->stream_has_enough_packets(vstate->audio_st,
								vstate->audio_stream, &vstate->audioq)
								&& obj->stream_has_enough_packets(vstate->video_st,
										vstate->video_stream, &vstate->videoq)
								&& obj->stream_has_enough_packets(vstate->subtitle_st,
										vstate->subtitle_stream, &vstate->subtitleq)))) {
			/* wait 10 ms */
			wait_mutex->lock();
			// SDL_CondWaitTimeout(is->continue_read_thread, wait_mutex, 10);
			OS::get_singleton()->delay_usec(10000); // 10000
//			vstate->continue_read_thread->wait();
			wait_mutex->unlock();
			continue;
		}

		if (!vstate->paused && (!vstate->audio_st || (vstate->auddec.finished == vstate->audioq.serial && obj->frame_queue_nb_remaining(&vstate->sampq) == 0)) && (!vstate->video_st || (vstate->viddec.finished == vstate->videoq.serial && obj->frame_queue_nb_remaining(&vstate->pictq) == 0))) {
			int& loop = obj->get_loop();
			if (loop != 1 && (!loop || --loop)) {
				obj->stream_seek(start_t != AV_NOPTS_VALUE ? start_t : 0, 0, 0);
			}
		}
		ret = av_read_frame(ic, pkt);
		if (ret < 0) {
			if ((ret == AVERROR_EOF || avio_feof(ic->pb)) && !vstate->eof) {
				if (vstate->video_stream >= 0) {
					obj->packet_queue_put_nullpacket(&vstate->videoq, pkt, vstate->video_stream);
				}
				if (vstate->audio_stream >= 0) {
					obj->packet_queue_put_nullpacket(&vstate->audioq, pkt, vstate->audio_stream);
				}
				if (vstate->subtitle_stream >= 0) {
					obj->packet_queue_put_nullpacket(&vstate->subtitleq, pkt, vstate->subtitle_stream);
				}
				vstate->eof = 1;
			}
			if (ic->pb && ic->pb->error) {
				break;
			}
			wait_mutex->lock();
			// SDL_CondWaitTimeout(is->continue_read_thread, wait_mutex, 10);
			OS::get_singleton()->delay_usec(10000); // 10000
//			vstate->continue_read_thread->wait();
			wait_mutex->unlock();
			continue;
		} else {
			vstate->eof = 0;
		}
		/* check if packet is in play range specified by user, then queue, otherwise discard */
		stream_start_time = ic->streams[pkt->stream_index]->start_time;
		pkt_ts = pkt->pts == AV_NOPTS_VALUE ? pkt->dts : pkt->pts;
		pkt_in_play_range = duration_t == AV_NOPTS_VALUE || (pkt_ts
						- (stream_start_time != AV_NOPTS_VALUE ?
								stream_start_time : 0))
						* av_q2d(ic->streams[pkt->stream_index]->time_base)
						- (double) (
								start_t != AV_NOPTS_VALUE ? start_t : 0)
								/ 1000000 <= ((double) duration_t / 1000000);
		if (pkt->stream_index == vstate->audio_stream && pkt_in_play_range) {
			obj->packet_queue_put(&vstate->audioq, pkt);
		} else if (pkt->stream_index == vstate->video_stream && pkt_in_play_range
				&& !(vstate->video_st->disposition & AV_DISPOSITION_ATTACHED_PIC)) {
			obj->packet_queue_put(&vstate->videoq, pkt);
		} else if (pkt->stream_index == vstate->subtitle_stream
				&& pkt_in_play_range) {
			obj->packet_queue_put(&vstate->subtitleq, pkt);
		} else {
			av_packet_unref(pkt);
		}
	}

	ret = 0;
	fail:
		if (ic && !vstate->ic) {
			avformat_close_input(&ic);
		}

	av_packet_free(&pkt);
	if (ret != 0) {
//        SDL_Event event;
//        event.type = FF_QUIT_EVENT;
//        event.user.data1 = is;
//        SDL_PushEvent(&event);
	}
//    SDL_DestroyMutex(wait_mutex);
	memdelete( wait_mutex );
//	return 0;

}

void FastForwardPlayback::stream_open(const char *filename, AVInputFormat *iformat) {

	vstate = static_cast<FastForwardPlayback::VideoState*>(av_mallocz(sizeof(VideoState)));
	if (!vstate) {
		return;
	}
	vstate->last_video_stream = vstate->video_stream = -1;
	vstate->last_audio_stream = vstate->audio_stream = -1;
	vstate->last_subtitle_stream = vstate->subtitle_stream = -1;
	vstate->filename = av_strdup(filename);
	if (!vstate->filename) {
		goto fail;
	}
	vstate->iformat = iformat;
	vstate->ytop = 0;
	vstate->xleft = 0;

	/* start video display */
	if (frame_queue_init(&vstate->pictq, &vstate->videoq, VIDEO_PICTURE_QUEUE_SIZE, 1) < 0) {
		goto fail;
	}
	if (frame_queue_init(&vstate->subpq, &vstate->subtitleq, SUBPICTURE_QUEUE_SIZE, 0) < 0) {
		goto fail;
	}
	if (frame_queue_init(&vstate->sampq, &vstate->audioq, SAMPLE_QUEUE_SIZE, 1) < 0) {
		goto fail;
	}
	if (packet_queue_init(&vstate->videoq) < 0 || packet_queue_init(&vstate->audioq) < 0 || packet_queue_init(&vstate->subtitleq) < 0) {
		goto fail;
	}

	vstate->continue_read_thread = memnew(Semaphore);

	init_clock(&vstate->vidclk, &vstate->videoq.serial);
	init_clock(&vstate->audclk, &vstate->audioq.serial);
	init_clock(&vstate->extclk, &vstate->extclk.serial);
	vstate->audio_clock_serial = -1;
//	if (startup_volume < 0)
//		av_log(NULL, AV_LOG_WARNING, "-volume=%d < 0, setting to 0\n",
//				startup_volume);
//	if (startup_volume > 100)
//		av_log(NULL, AV_LOG_WARNING, "-volume=%d > 100, setting to 100\n",
//				startup_volume);
//	startup_volume = av_clip(startup_volume, 0, 100);
//	startup_volume = av_clip(SDL_MIX_MAXVOLUME * startup_volume / 100, 0,
//			SDL_MIX_MAXVOLUME);
	vstate->audio_volume = MIX_MAXVOLUME;
	vstate->muted = 0;
	vstate->av_sync_type = av_sync_type;
	vstate->show_mode = show_mode;
	vstate->read_tid = memnew(Thread);
	vstate->read_tid->start( _read_thread, this );
	if (!vstate->read_tid->is_started()) {
		fail:
			stream_close();
	}
}

void FastForwardPlayback::stream_cycle_channel(int codec_type) {

	AVFormatContext *ic = vstate->ic;
	int start_index, stream_index;
	int old_index;
	AVStream *st;
	AVProgram *p = NULL;
	int nb_streams = vstate->ic->nb_streams;

	if (codec_type == AVMEDIA_TYPE_VIDEO) {
		start_index = vstate->last_video_stream;
		old_index = vstate->video_stream;
	} else if (codec_type == AVMEDIA_TYPE_AUDIO) {
		start_index = vstate->last_audio_stream;
		old_index = vstate->audio_stream;
	} else {
		start_index = vstate->last_subtitle_stream;
		old_index = vstate->subtitle_stream;
	}
	stream_index = start_index;

	if (codec_type != AVMEDIA_TYPE_VIDEO && vstate->video_stream != -1) {
		p = av_find_program_from_stream(ic, NULL, vstate->video_stream);
		if (p) {
			nb_streams = p->nb_stream_indexes;
			for (start_index = 0; start_index < nb_streams; start_index++)
				if (p->stream_index[start_index] == stream_index)
					break;
			if (start_index == nb_streams)
				start_index = -1;
			stream_index = start_index;
		}
	}

	for (;;) {
		if (++stream_index >= nb_streams) {
			if (codec_type == AVMEDIA_TYPE_SUBTITLE) {
				stream_index = -1;
				vstate->last_subtitle_stream = -1;
				goto the_end;
			}
			if (start_index == -1)
				return;
			stream_index = 0;
		}
		if (stream_index == start_index)
			return;
		st = vstate->ic->streams[p ? p->stream_index[stream_index] : stream_index];
		if (st->codecpar->codec_type == codec_type) {
			/* check that parameters are OK */
			switch (codec_type) {
			case AVMEDIA_TYPE_AUDIO:
				if (st->codecpar->sample_rate != 0
						&& st->codecpar->channels != 0)
					goto the_end;
				break;
			case AVMEDIA_TYPE_VIDEO:
			case AVMEDIA_TYPE_SUBTITLE:
				goto the_end;
			default:
				break;
			}
		}
	}
	the_end: if (p && stream_index != -1)
		stream_index = p->stream_index[stream_index];
	av_log(NULL, AV_LOG_INFO, "Switch %s stream from #%d to #%d\n", av_get_media_type_string(AVMediaType(codec_type)), old_index, stream_index);

	stream_component_close(old_index);
	stream_component_open(stream_index);

}

void FastForwardPlayback::toggle_full_screen() {
	is_full_screen = !is_full_screen;
//    SDL_SetWindowFullscreen(window, is_full_screen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
}

void FastForwardPlayback::toggle_audio_display() {

	int next = vstate->show_mode;
	do {
		next = (next + 1) % SHOW_MODE_NB;
	} while (next != vstate->show_mode
			&& (next == SHOW_MODE_VIDEO && !vstate->video_st
					|| next != SHOW_MODE_VIDEO && !vstate->audio_st));
	if (vstate->show_mode != next) {
		vstate->force_refresh = 1;
		vstate->show_mode = FastForwardPlayback::ShowMode(next);
	}

}

void FastForwardPlayback::refresh_loop_wait_event(int event) {

	double remaining_time = 0.0;
	if (remaining_time > 0.0) {
		av_usleep((int64_t)(remaining_time * 1000000.0));
	}
	remaining_time = REFRESH_RATE;
	if (vstate->show_mode != SHOW_MODE_NONE && (!vstate->paused || vstate->force_refresh)) {
		video_refresh(&remaining_time);
	}

}

void FastForwardPlayback::seek_chapter(int incr) {

	int64_t pos = get_master_clock() * AV_TIME_BASE;
	int i;
	if (!vstate->ic->nb_chapters) {
		return;
	}
	/* find the current chapter */
	for (i = 0; i < vstate->ic->nb_chapters; i++) {
		AVChapter *ch = vstate->ic->chapters[i];
		if (av_compare_ts(pos, AV_TIME_BASE_Q, ch->start, ch->time_base) < 0) {
			i--;
			break;
		}
	}
	i += incr;
	i = FFMAX(i, 0);
	if (i >= vstate->ic->nb_chapters) {
		return;
	}
	av_log(NULL, AV_LOG_VERBOSE, "Seeking to chapter %d.\n", i);
	stream_seek( av_rescale_q(vstate->ic->chapters[i]->start, vstate->ic->chapters[i]->time_base, AV_TIME_BASE_Q), 0, 0);

}

void FastForwardPlayback::do_seek(double incr) {

	double pos;
	if (seek_by_bytes) {
		pos = -1;
		if (pos < 0 && vstate->video_stream >= 0)
			pos = frame_queue_last_pos(&vstate->pictq);
		if (pos < 0 && vstate->audio_stream >= 0)
			pos = frame_queue_last_pos(&vstate->sampq);
		if (pos < 0)
			pos = avio_tell(vstate->ic->pb);
		if (vstate->ic->bit_rate)
			incr *= vstate->ic->bit_rate / 8.0;
		else
			incr *= 180000.0;
		pos += incr;
		stream_seek(pos, incr, 1);
	} else {
		pos = get_master_clock();
		if (isnan(pos))
			pos = (double) vstate->seek_pos / AV_TIME_BASE;
		pos += incr;
		if (vstate->ic->start_time != AV_NOPTS_VALUE
				&& pos < vstate->ic->start_time / (double) AV_TIME_BASE)
			pos = vstate->ic->start_time / (double) AV_TIME_BASE;
		stream_seek((int64_t)(pos * AV_TIME_BASE), (int64_t)(incr * AV_TIME_BASE), 0);
	}

}

/* handle an event sent by the GUI */
void FastForwardPlayback::event_loop() {

	upload_mutex.lock();
	local_events.append_array( events );
	events.clear();
	upload_mutex.unlock();

	int event_count = local_events.size();

	if ( vstate != NULL ) {

		refresh_loop_wait_event(0);

		double incr, pos, frac;

		for (int i = 0; i < event_count; ++i) {

			double x;
			// If we don't yet have a window, skip all key events, because read_thread might still be initializing...
			if (!vstate->width) {
				continue;
			}

			switch (local_events[i]) {

				case FFP_QUIT:
					upload_run = false;
					do_quit();
					local_events.clear();
					// making sure we leave the loop after this event
					return;

				case FFP_FULLSCREEN:
					toggle_full_screen();
					vstate->force_refresh = 1;
					break;

				case FFP_PAUSE:
					toggle_pause();
					break;

				case FFP_MUTE:
					toggle_mute();
					break;

				case FFP_VOLUME_UP:
					update_volume(1, 1);
					break;

				case FFP_VOLUME_DOWN:
					update_volume(-1, 1);
					break;

				case FFP_NEXT_FRAME: // S: Step to next frame
					step_to_next_frame();
					break;

				case FFP_NEXT_AUDIO:
					stream_cycle_channel(AVMEDIA_TYPE_AUDIO);
					break;
				case FFP_NEXT_VIDEO:
					stream_cycle_channel(AVMEDIA_TYPE_VIDEO);
					break;

				case FFP_NEXT_ALL:
					stream_cycle_channel(AVMEDIA_TYPE_VIDEO);
					stream_cycle_channel(AVMEDIA_TYPE_AUDIO);
					stream_cycle_channel(AVMEDIA_TYPE_SUBTITLE);
					break;

				case FFP_NEXT_SUBTITLE:
					stream_cycle_channel(AVMEDIA_TYPE_SUBTITLE);
					break;

				case FFP_AUDIODISPLAY:
					toggle_audio_display();
					break;

				case FFP_NEXT_CHAPTER:
					if (vstate->ic->nb_chapters <= 1) {
						incr = 600.0;
						do_seek(incr);
					}
					seek_chapter(1);
					break;

				case FFP_PREVIOUS_CHAPTER:
					if (vstate->ic->nb_chapters <= 1) {
						incr = -600.0;
						do_seek(incr);
					}
					seek_chapter(-1);
					break;

				case FFP_JUMP_LEFT:
					incr = seek_interval ? -seek_interval : -10.0;
					do_seek(incr);
					break;

				case FFP_JUMP_RIGHT:
					incr = seek_interval ? seek_interval : 10.0;
					do_seek(incr);
					break;

				default:
					break;

			}
		}

	} else {

		for (int i = 0; i < event_count; ++i) {
			if ( local_events[i] == FFP_QUIT ) {
				upload_run = false;
				return;
			}
		}

	}

	local_events.clear();

	upload_mutex.lock();
	playback_position = get_master_clock();
	upload_mutex.unlock();

}

Ref<ImageTexture> FastForwardPlayback::get_vis_texture() {
	return vis_texture;
}

Ref<ImageTexture> FastForwardPlayback::get_sub_texture() {
	return sub_texture;
}

Ref<ImageTexture> FastForwardPlayback::get_vid_texture() {
	return vid_texture;
}

void FastForwardPlayback::load( String abs_path ) {

	stop();

	filepath = abs_path;
	playback_position = 0;

	if ( vid_texture.is_null() ) {
		vid_texture.instance();
		// let's create a 1x1 texture at startup to link it in video player
		vid_texture->create( 1, 1, Image::FORMAT_RGBA8, 0);
	}

}

void FastForwardPlayback::stop() {

	if (vstate) {
		push_event( FFP_QUIT );
		upload_thread.wait_to_finish();
	}

};

void FastForwardPlayback::play() {

	// player already loaded
	if ( vstate != NULL ) {

		if ( is_paused() ) {
			set_paused(false);
		}

	// no player yet, let's start one
	} else {

		if ( filepath.empty() ) {
			return;
		}

		std::wstring ws = filepath.c_str();
		std::string s = std::string(ws.begin(), ws.end());
		const char *filename = s.c_str();
		stream_open( filename, file_iformat );

	}

	events.clear();

	// and let's launch the upload thread
	if ( !upload_run ) {
		upload_run = true;
		upload_thread.start( _upload_thread, this);
	}

};

bool FastForwardPlayback::is_playing() const {
	return vstate != NULL;
};

void FastForwardPlayback::set_paused(bool p_paused) {
	if ( vstate == NULL ) {
		return;
	}
	if ( ( vstate->paused && !p_paused ) || ( !vstate->paused && p_paused ) ) {
		push_event( FFP_PAUSE );
	}
};

bool FastForwardPlayback::is_paused() const {
	if ( vstate == NULL ) {
		return false;
	}
	return vstate->paused;
};

void FastForwardPlayback::set_loop(bool p_enable) {
	loop = p_enable;
};

bool FastForwardPlayback::has_loop() const {
	return loop;
};

float FastForwardPlayback::get_length() const {

	if ( vstate == NULL ) {
		return 0;
	}
	if ( vstate->ic == NULL ) {
		return 0;
	}
	return vstate->ic->duration / 1000000LL;

};

float FastForwardPlayback::get_playback_position() const {

	upload_mutex.lock();
	double pp = playback_position;
	upload_mutex.unlock();
	return pp;

};

void FastForwardPlayback::seek(float p_time) {

	if ( vstate == NULL ) {
		// stream is not opened!
		return;
	}
	if ( vstate->ic == NULL ) {
		// stream is not opened!
		return;
	}

	double frac = double(p_time) * 1000000LL / double(vstate->ic->duration);
	if ( frac < 0 ) { frac = 0; }
	else if ( frac > 1 ) { frac = 1; }

	int64_t ts;
	int ns, hh, mm, ss;
	int tns, thh, tmm, tss;
	tns  = vstate->ic->duration / 1000000LL;
	thh  = tns / 3600;
	tmm  = (tns % 3600) / 60;
	tss  = (tns % 60);
	ns   = frac * tns;
	hh   = ns / 3600;
	mm   = (ns % 3600) / 60;
	ss   = (ns % 60);
	av_log(NULL, AV_LOG_INFO,
		   "Seek to %2.0f%% (%2d:%02d:%02d) of total duration (%2d:%02d:%02d)       \n", frac*100,
			hh, mm, ss, thh, tmm, tss);
	ts = frac * vstate->ic->duration;
	if (vstate->ic->start_time != AV_NOPTS_VALUE)
		ts += vstate->ic->start_time;

	std::cout << "seek " << frac << " : " << ts << " / " <<  vstate->ic->duration << std::endl;

	stream_seek(ts, 0, 0);

}

Ref<Texture> FastForwardPlayback::get_texture() const {
	return vid_texture;
};

void FastForwardPlayback::set_audio_track(int p_idx) {};

void FastForwardPlayback::set_mix_callback(AudioMixCallback p_callback, void *p_userdata) {};

int FastForwardPlayback::get_channels() const { return 0; };

int FastForwardPlayback::get_mix_rate() const { return 0; };

void FastForwardPlayback::set_upload_run( bool run ) {
	upload_run = run;
}

void FastForwardPlayback::_upload_thread( void * arg ) {

	FastForwardPlayback * obj = reinterpret_cast<FastForwardPlayback *>(arg);
	uint64_t now = OS::get_singleton()->get_ticks_usec();

	while ( obj->is_playing() && obj->is_upload_running() ) {
		obj->event_loop();
		uint64_t new_now = OS::get_singleton()->get_ticks_usec();
		uint64_t wait = new_now - now;
		if ( wait < ( UPLOAD_THREAD_MIN_DELAY - 1000 ) ) {
			// let's wait a bit to run it again
			OS::get_singleton()->delay_usec( UPLOAD_THREAD_MIN_DELAY - wait );
		}
		now = new_now;
	}

	obj->set_upload_run( false );

}

void FastForwardPlayback::update(float p_delta) {
}

FastForwardPlayback::FastForwardPlayback() :
		vstate(NULL),
		sws_dict(NULL),
		swr_opts(NULL),
		format_opts(NULL),
		codec_opts(NULL),
		resample_opts(NULL),
		file_iformat(NULL),
		input_filename(NULL),
		audio_disable(1),
		video_disable(0),
		subtitle_disable(1),
		seek_by_bytes(-1),
		seek_interval(10),
		show_status(-1),
		start_time( AV_NOPTS_VALUE ),
		duration( AV_NOPTS_VALUE ),
		fast(0),
		genpts(0),
		lowres(0),
		decoder_reorder_pts(-1),
		loop(1),
		framedrop(-1),
		infinite_buffer(-1),
		audio_codec_name(NULL),
		subtitle_codec_name(NULL),
		video_codec_name(NULL),
		rdftspeed(0.02),
//		vfilters_list(NULL),
//		nb_vfilters(0),
//		afilters(NULL),
		autorotate(1),
		find_stream_info(1),
		filter_nbthreads(0),
		is_full_screen(false),
		audio_callback_time(0),
		default_width(640),
		default_height(480),
		screen_width(0),
		screen_height(0),
		transfer_frame(NULL),
		sws_flags(SWS_BICUBIC),
		upload_run(false)
		{

//	av_sync_type = SyncMode::AV_SYNC_VIDEO_MASTER;
	av_sync_type = SyncMode::AV_SYNC_EXTERNAL_CLOCK;
	show_mode = ShowMode::SHOW_MODE_VIDEO;
	av_dict_set(&sws_dict, "flags", "bicubic", 0);

}

FastForwardPlayback::~FastForwardPlayback() {
	stop();
}

