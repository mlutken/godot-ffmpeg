extra flags for SCons configuration in eclipse

# godot 3.x

**linux**

platform=x11 tools=yes debug_symbols=yes target=debug bits=64


**windows**

-u --jobs=8  platform=windows tools=yes debug_symbols=yes target=debug bits=64
