/*
 * video_stream_generic.h
 *
 *  Created on: Sep 29, 2020
 *      Author: frankiezafe
 */

#ifndef _VIDEO_STREAM_GENERIC
#define _VIDEO_STREAM_GENERIC

#define __STDINT_MACROS

#include <iostream>

// godot includes
#include "core/object.h"
#include "core/io/resource_loader.h"
#include "core/os/file_access.h"
#include "core/os/semaphore.h"
#include "core/os/thread.h"
#include "core/ring_buffer.h"
#include "scene/resources/video_stream.h"
#include "servers/audio_server.h"

// module includes
#include "fast_forward_playback.h"
//#include "common.h"
//#include "video_stream_playback_generic.h"

class FastForwardStream: public VideoStream {

GDCLASS(FastForwardStream, VideoStream);

private:

	String file;
	Ref<FastForwardPlayback> player;

protected:
	static void _bind_methods();

public:

	Ref<VideoStreamPlayback> instance_playback();

	void seek(const real_t &p );

	real_t get_duration();

	void set_file(const String &p_file);
	String get_file();

	Ref<Texture> get_texture();
	void update(float p_delta);

	void set_audio_track(int p_track);

	FastForwardStream();
	virtual ~FastForwardStream();

};

#endif /* _VIDEO_STREAM_GENERIC */
