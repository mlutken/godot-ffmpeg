/*
 * resource_loader_video_generic.cpp
 *
 *  Created on: Oct 6, 2020
 *      Author: frankiezafe
 */

#include "fast_forward_loader.h"

static const uint8_t ffmpeg_EXTENSION_SIZE = 17;
static const String ffmpeg_extensions[ 17 ] = {
	"mp4", "m4p", "m4v", "xvid", "avchd",
	"avi", "wmv",
	"mkv",
	"mov", "qt",
	"flv", "swf",
	"mpg", "mp2", "mpeg", "mpe", "mpv"
};

RES FastForwardLoader::load(const String &p_path, const String &p_original_path, Error *r_error) {

	FileAccess *f = FileAccess::open(p_path, FileAccess::READ);
	if (!f) {
		if (r_error) {
			*r_error = ERR_CANT_OPEN;
		}
		return RES();
	}

	FastForwardStream *stream = memnew(FastForwardStream);
	stream->set_file(f->get_path_absolute());

	Ref<FastForwardStream> rstream = Ref<FastForwardStream>(stream);

	if (r_error) {
		*r_error = OK;
	}

	f->close();
	memdelete(f);
	return rstream;

}

void FastForwardLoader::get_recognized_extensions( List<String> *p_extensions) const {
	for ( int i = 0; i < ffmpeg_EXTENSION_SIZE; ++i ) {
		p_extensions->push_back(ffmpeg_extensions[i]);
	}
}

bool FastForwardLoader::handles_type(const String &p_type) const {
	return ClassDB::is_parent_class(p_type, "VideoStream");
}

String FastForwardLoader::get_resource_type( const String &p_path) const {
	String el = p_path.get_extension().to_lower();
	for ( int i = 0; i < ffmpeg_EXTENSION_SIZE; ++i ) {
		if ( el == ffmpeg_extensions[i] ) {
			return "FastForwardStream";
		}
	}
	return "";
}
