#!/bin/sh

cd ../ffmpeg/

git checkout n4.4

rm -f config.h
echo "Building ffmpeg release for Linux"

set -e

ARCH="x86_64"

# --enable-debug=0
# --enable-nonfree
# --prefix=./bin/linux/release/${ARCH}

GENERAL="
	--enable-pthreads \
	--disable-shared \
	--enable-static"

# todo: reacitvate zlib
MODULES="\
	--disable-zlib \
	--disable-programs \
	--disable-doc \
	--disable-manpages \
	--disable-podpages \
	--disable-txtpages \
	--disable-ffplay \
	--disable-ffprobe \
	--disable-ffmpeg"

VIDEO_DECODERS=""

AUDIO_DECODERS=""

DEMUXERS=""

VIDEO_ENCODERS=""

AUDIO_ENCODERS=""

MUXERS=""

PARSERS=""

PROTOCOLS="\
	--enable-protocol=file"

./configure \
	--prefix=./../lib/linux/release/${ARCH} \
	${GENERAL} \
	--extra-cflags="-D__STDC_CONSTANT_MACROS -O3" \
	--enable-zlib \
	--enable-pic \
	--disable-yasm \
	${MODULES} \
	${VIDEO_DECODERS} \
	${AUDIO_DECODERS} \
	${VIDEO_ENCODERS} \
	${AUDIO_ENCODERS} \
	${DEMUXERS} \
	${MUXERS} \
	${PARSERS} \
	${PROTOCOLS} \
	--arch=${ARCH}

make clean
make install
