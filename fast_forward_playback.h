/*
 * ffplay.h
 *
 *  Created on: Aug 19, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_FFMPEG_FAST_FORWARD_PLAYBACK_H_
#define MODULES_FFMPEG_FAST_FORWARD_PLAYBACK_H_

#include <stdint.h>
#include <iostream>
#include "core/os/semaphore.h"
#include "core/os/os.h"
#include "core/os/thread.h"
#include "core/os/mutex.h"
#include "core/pool_vector.h"
#include "core/color.h"
#include "core/image.h"
#include "scene/main/node.h"
#include "scene/resources/texture.h"
#include "scene/resources/video_stream.h"

extern "C" {

	#include "libavcodec/avcodec.h"
	#include "libavutil/avstring.h"
	#include "libavutil/eval.h"
	#include "libavutil/mathematics.h"
	#include "libavutil/pixdesc.h"
	#include "libavutil/imgutils.h"
	#include "libavutil/dict.h"
	#include "libavutil/fifo.h"
	#include "libavutil/parseutils.h"
	#include "libavutil/samplefmt.h"
	#include "libavutil/avassert.h"
	#include "libavutil/time.h"
	#include "libavutil/bprint.h"
	#include "libavformat/avformat.h"
	#include "libswscale/swscale.h"
	#include "libavutil/opt.h"
	#include "libavcodec/avfft.h"
	#include "libswresample/swresample.h"
	#include "libavdevice/avdevice.h"
//	#include "libavfilter/avfilter.h"
//	#include "libavfilter/buffersink.h"
//	#include "libavfilter/buffersrc.h"

}

#ifndef NAN
#define NAN av_int2float(0x7fc00000)
#endif

#define MIX_MAXVOLUME 100

#define MAX_QUEUE_SIZE (15 * 1024 * 1024)
#define MIN_FRAMES 25
#define EXTERNAL_CLOCK_MIN_FRAMES 2
#define EXTERNAL_CLOCK_MAX_FRAMES 10

/* no AV sync correction is done if below the minimum AV sync threshold */
#define AV_SYNC_THRESHOLD_MIN 0.04
/* AV sync correction is done if above the maximum AV sync threshold */
#define AV_SYNC_THRESHOLD_MAX 0.1
/* If a frame duration is longer than this, it will not be duplicated to compensate AV sync */
#define AV_SYNC_FRAMEDUP_THRESHOLD 0.1
/* no AV correction is done if too big error */
#define AV_NOSYNC_THRESHOLD 10.0
/* maximum audio speed change to get correct sync */
#define SAMPLE_CORRECTION_PERCENT_MAX 10
/* external clock speed adjustment constants for realtime sources based on buffer fullness */
#define EXTERNAL_CLOCK_SPEED_MIN  0.900
#define EXTERNAL_CLOCK_SPEED_MAX  1.010
#define EXTERNAL_CLOCK_SPEED_STEP 0.001
/* we use about AUDIO_DIFF_AVG_NB A-V differences to make the average */
#define AUDIO_DIFF_AVG_NB   20
/* polls for possible required screen refresh at least this often, should be less than 1/fps */
#define REFRESH_RATE 0.01
/* NOTE: the size must be big enough to compensate the hardware audio buffersize size */
/* TODO: We assume that a decoded and resampled frame fits into this buffer */
#define SAMPLE_ARRAY_SIZE (8 * 65536)

#define VIDEO_PICTURE_QUEUE_SIZE 3
#define SUBPICTURE_QUEUE_SIZE 16
#define SAMPLE_QUEUE_SIZE 9
#define FRAME_QUEUE_SIZE FFMAX(SAMPLE_QUEUE_SIZE, FFMAX(VIDEO_PICTURE_QUEUE_SIZE, SUBPICTURE_QUEUE_SIZE))

// number of microseconds to wait between 2 calls to upload
#define UPLOAD_THREAD_MIN_DELAY 16600 // ~60 fps

class FastForwardPlayback : public VideoStreamPlayback {

	GDCLASS(FastForwardPlayback, VideoStreamPlayback);

public:

	typedef struct MyAVPacketList {
		AVPacket *pkt;
		int serial;
	} MyAVPacketList;

	typedef struct PacketQueue {
		AVFifoBuffer *pkt_list;
		int nb_packets;
		int size;
		int64_t duration;
		int abort_request;
		int serial;
		Mutex* pq_mutex;
		Semaphore* pq_cond;
	} PacketQueue;

	typedef struct AudioParams {
		int freq;
		int channels;
		int64_t channel_layout;
		enum AVSampleFormat fmt;
		int frame_size;
		int bytes_per_sec;
	} AudioParams;

	typedef struct Clock {
		double pts; /* clock base */
		double pts_drift; /* clock base minus time at which we updated the clock */
		double last_updated;
		double speed;
		int serial; /* clock is based on a packet with this serial */
		int paused;
		int *queue_serial; /* pointer to the current packet queue serial, used for obsolete clock detection */
	} Clock;

	/* Common struct for handling all types of decoded data and allocated render buffers. */
	typedef struct Frame {
		AVFrame *frame;
		AVSubtitle sub;
		int serial;
		double pts; /* presentation timestamp for the frame */
		double duration; /* estimated duration of the frame */
		int64_t pos; /* byte position of the frame in the input file */
		int width;
		int height;
		int format;
		AVRational sar;
		int uploaded;
		int flip_v;
	} Frame;

	typedef struct FrameQueue {
		Frame queue[FRAME_QUEUE_SIZE];
		int rindex;
		int windex;
		int size;
		int max_size;
		int keep_last;
		int rindex_shown;
		Mutex* fq_mutex;
		Semaphore* fq_cond;
		PacketQueue *pktq;
	} FrameQueue;

	enum SyncMode {
		AV_SYNC_AUDIO_MASTER, /* default choice */
		AV_SYNC_VIDEO_MASTER,
		AV_SYNC_EXTERNAL_CLOCK, /* synchronize to an external clock */
	};

	typedef struct Decoder {
		AVPacket *pkt;
		PacketQueue *queue;
		AVCodecContext *avctx;
		int pkt_serial;
		int finished;
		int packet_pending;
		Semaphore* empty_queue_cond;
		int64_t start_pts;
		AVRational start_pts_tb;
		int64_t next_pts;
		AVRational next_pts_tb;
		Thread* decoder_tid;
	} Decoder;

	enum ShowMode {
		SHOW_MODE_NONE = -1,
		SHOW_MODE_VIDEO = 0,
		SHOW_MODE_WAVES,
		SHOW_MODE_RDFT,
		SHOW_MODE_NB
	};

	typedef struct VideoState {

		Thread* read_tid;
		AVInputFormat *iformat;
		int abort_request;
		int force_refresh;
		int paused;
		int last_paused;
		int queue_attachments_req;
		int seek_req;
		int seek_flags;
		int64_t seek_pos;
		int64_t seek_rel;
		int read_pause_return;
		AVFormatContext *ic;
		int realtime;

		Clock audclk;
		Clock vidclk;
		Clock extclk;

		FrameQueue pictq;
		FrameQueue subpq;
		FrameQueue sampq;

		Decoder auddec;
		Decoder viddec;
		Decoder subdec;

		int audio_stream;

		int av_sync_type;

		double audio_clock;
		int audio_clock_serial;
		double audio_diff_cum; /* used for AV difference average computation */
		double audio_diff_avg_coef;
		double audio_diff_threshold;
		int audio_diff_avg_count;
		AVStream *audio_st;
		PacketQueue audioq;
		int audio_hw_buf_size;
		uint8_t *audio_buf;
		uint8_t *audio_buf1;
		unsigned int audio_buf_size; /* in bytes */
		unsigned int audio_buf1_size;
		int audio_buf_index; /* in bytes */
		int audio_write_buf_size;
		int audio_volume;
		int muted;
		AudioParams audio_src;
		AudioParams audio_tgt;
		struct SwrContext *swr_ctx;
		int frame_drops_early;
		int frame_drops_late;
		ShowMode show_mode;
		int16_t sample_array[SAMPLE_ARRAY_SIZE];
		int sample_array_index;
		int last_i_start;
		RDFTContext *rdft;
		int rdft_bits;
		FFTSample *rdft_data;
		int xpos;
		double last_vis_time;

		int subtitle_stream;
		AVStream *subtitle_st;
		PacketQueue subtitleq;

		double frame_timer;
		double frame_last_returned_time;
		double frame_last_filter_delay;
		int video_stream;
		AVStream *video_st;
		PacketQueue videoq;
		double max_frame_duration; // maximum duration of a frame - above this, we consider the jump a timestamp discontinuity
		SwsContext *img_convert_ctx;
		SwsContext *sub_convert_ctx;
		int eof;

		char *filename;
		int width, height, xleft, ytop;
		int step;
		int last_video_stream, last_audio_stream, last_subtitle_stream;

		Semaphore* continue_read_thread;

	} VideoState;

	enum FFPLAY_EVENTS {
		FFP_QUIT,
		FFP_FULLSCREEN,
		FFP_PAUSE,
		FFP_MUTE,
		FFP_VOLUME_UP,
		FFP_VOLUME_DOWN,
		FFP_NEXT_FRAME,
		FFP_NEXT_AUDIO,
		FFP_NEXT_VIDEO,
		FFP_NEXT_ALL,
		FFP_NEXT_SUBTITLE,
		FFP_AUDIODISPLAY,
		FFP_NEXT_CHAPTER,
		FFP_PREVIOUS_CHAPTER,
		FFP_JUMP_LEFT,
		FFP_JUMP_RIGHT,
		FFP_SEEK,
	};

private:

	VideoState* vstate;

	AVDictionary *sws_dict;
	AVDictionary *swr_opts;
	AVDictionary *format_opts;
	AVDictionary *codec_opts;
	AVDictionary *resample_opts;

	AVInputFormat *file_iformat;
	const char *input_filename;
	int audio_disable;
	int video_disable;
	int subtitle_disable;
	int seek_by_bytes;
	float seek_interval;
//	int display_disable;
	int show_status;
	int64_t start_time;
	int64_t duration;
	int fast;
	int genpts;
	int lowres;
	int decoder_reorder_pts;
	int loop;
	int framedrop;
	int infinite_buffer;
	const char *audio_codec_name;
	const char *subtitle_codec_name;
	const char *video_codec_name;
	double rdftspeed;
//	const char **vfilters_list;
//	int nb_vfilters;
//	char *afilters = NULL;
	int autorotate;
	int find_stream_info;
	int filter_nbthreads;
	bool is_full_screen;
	int64_t audio_callback_time;
	SyncMode av_sync_type;
	ShowMode show_mode;
	int default_width;
	int default_height;
	int screen_width;
	int screen_height;

	Vector<FFPLAY_EVENTS> local_events;
	Vector<FFPLAY_EVENTS> events;

	Ref<ImageTexture> vis_texture;
	Ref<ImageTexture> sub_texture;
	Ref<ImageTexture> vid_texture;
	PoolVector<uint8_t> image_buffer;
	AVFrame * transfer_frame;
	int sws_flags;

	String filepath;
	double playback_position;

	Mutex upload_mutex;
	Thread upload_thread;
	volatile bool upload_run;

protected:

	_FORCE_INLINE_ const bool is_upload_running() const { return upload_run; }
	void set_upload_run( bool run );

public:

	FastForwardPlayback::VideoState* get_videostate() const { return vstate; }
	AVDictionary* get_sws_dict() const { return sws_dict; }
	AVDictionary* get_swr_opts() const { return swr_opts; }
	AVDictionary* get_format_opts() const { return format_opts; }
	AVDictionary* get_codec_opts() const { return codec_opts; }
	AVDictionary* get_resample_opts() const { return resample_opts; }

	const int get_audio_disable() const { return audio_disable; }
	const int get_video_disable() const { return video_disable; }
	const int get_subtitle_disable() const { return subtitle_disable; }
	const int64_t get_start_time() const { return start_time; }
	const int64_t get_duration() const { return duration; }
	int& get_seek_by_bytes() { return seek_by_bytes; }
	const int get_genpts() const { return genpts; }
	const int get_find_stream_info() const { return find_stream_info; }
	const int get_filter_nbthreads() const { return filter_nbthreads; }
	const int get_show_status() const { return show_status; }
	const int get_infinite_buffer() const { return infinite_buffer; }
	void set_infinite_buffer( int i ) { infinite_buffer = i; }
	int& get_loop() { return loop; }

	void push_event( FFPLAY_EVENTS ev );

	static void _upload_thread( void * arg );
	static void _read_thread( void * arg );
	static void _audio_thread( void * arg );
	static void _video_thread( void * arg );
	static void _subtitle_thread( void * arg );
	static int _decode_interrupt_cb(void *ctx);

	static inline Image::Format convert_px_format_godot( AVPixelFormat frmt );
	static inline AVPixelFormat convert_px_format_av( Image::Format frmt );
	static inline int check_stream_specifier(AVFormatContext *s, AVStream *st, const char *spec);
	static inline AVDictionary * filter_codec_opts(AVDictionary *opts, enum AVCodecID codec_id, AVFormatContext *s, AVStream *st, const AVCodec *codec);
	static inline AVDictionary **setup_find_stream_info_opts(AVFormatContext *s, AVDictionary *codec_opts);
	static inline void* grow_array(void *array, int elem_size, int *size, int new_size);
//	int opt_add_vfilter(void *optctx, const char *opt, const char *arg);
	static inline int cmp_audio_fmts(enum AVSampleFormat fmt1, int64_t channel_count1, enum AVSampleFormat fmt2, int64_t channel_count2);
	static int is_realtime(AVFormatContext *s);

	inline int64_t get_valid_channel_layout(int64_t channel_layout, int channels);
	int packet_queue_put_private(PacketQueue *q, AVPacket *pkt);
	int packet_queue_put(PacketQueue *q, AVPacket *pkt);
	int packet_queue_put_nullpacket(PacketQueue *q, AVPacket *pkt, int stream_index);
	int packet_queue_init(PacketQueue *q);
	void packet_queue_flush(PacketQueue *q);
	void packet_queue_destroy(PacketQueue *q);
	void packet_queue_abort(PacketQueue *q);
	void packet_queue_start(PacketQueue *q);
	int packet_queue_get(PacketQueue *q, AVPacket *pkt, int block, int *serial);
	int decoder_init(Decoder *d, AVCodecContext *avctx, PacketQueue *queue, Semaphore *empty_queue_cond);
	int decoder_decode_frame(Decoder *d, AVFrame *frame, AVSubtitle *sub);
	void decoder_destroy(Decoder *d);
	void frame_queue_unref_item(Frame *vp);
	int frame_queue_init(FrameQueue *f, PacketQueue *pktq, int max_size, int keep_last);
	void frame_queue_destory(FrameQueue *f);
	void frame_queue_signal(FrameQueue *f);
	FastForwardPlayback::Frame* frame_queue_peek(FrameQueue *f);
	FastForwardPlayback::Frame* frame_queue_peek_next(FrameQueue *f);
	FastForwardPlayback::Frame* frame_queue_peek_last(FrameQueue *f);
	FastForwardPlayback::Frame* frame_queue_peek_writable(FrameQueue *f);
	FastForwardPlayback::Frame* frame_queue_peek_readable(FrameQueue *f);
	void frame_queue_push(FrameQueue *f);
	void frame_queue_next(FrameQueue *f);
	int frame_queue_nb_remaining(FrameQueue *f);
	int64_t frame_queue_last_pos(FrameQueue *f);
	void decoder_abort(Decoder *d, FrameQueue *fq);
	inline void fill_rectangle(int x, int y, int w, int h);
	int realloc_texture();
	void calculate_display_rect(Rect2 *rect, int scr_xleft, int scr_ytop, int scr_width, int scr_height, int pic_width, int pic_height, AVRational pic_sar);
	void get_godot_pix_fmt(int format, int *godot_pix_fmt);
	int upload_texture(AVFrame *frame, SwsContext **img_convert_ctx);
	void set_godot_yuv_conversion_mode(AVFrame *frame);
	void video_image_display();
	inline int compute_mod(int a, int b);
	void video_audio_display();
	void stream_component_close(int stream_index);
	void framequeue_close( FrameQueue* fq );
	void stream_close();
	void do_quit();
	int video_open();
	void video_display();
	double get_clock(Clock *c);
	void set_clock_at(Clock *c, double pts, int serial, double time);
	void set_clock(Clock *c, double pts, int serial);
	void set_clock_speed(Clock *c, double speed);
	void init_clock(Clock *c, int *queue_serial);
	void sync_clock_to_slave(Clock *c, Clock *slave);
	int get_master_sync_type();
	double get_master_clock();
	void check_external_clock_speed();
	void stream_seek(int64_t pos, int64_t rel, int seek_by_bytes);
	void stream_toggle_pause();
	void toggle_pause();
	void toggle_mute();
	void update_volume(int sign, double step);
	void step_to_next_frame();
	double compute_target_delay(double delay);
	double vp_duration(Frame *vp, Frame *nextvp);
	void update_video_pts(double pts, int64_t pos, int serial);
	void video_refresh(double *remaining_time);
	int queue_picture(AVFrame *src_frame, double pts, double duration, int64_t pos, int serial);
	int get_video_frame(AVFrame *frame);
//	int decoder_start(Decoder *d, int (*fn)(void*), const char *thread_name, void *arg);
	void update_sample_display(short *samples,int samples_size);
	int synchronize_audio(int nb_samples);
	int audio_decode_frame();
	void godot_audio_callback(void *opaque, uint8_t *stream, int len);
	int audio_open(int64_t wanted_channel_layout,int wanted_nb_channels, int wanted_sample_rate,AudioParams *audio_hw_params);
	int stream_component_open(int stream_index);
	int stream_has_enough_packets(AVStream *st, int stream_id,PacketQueue *queue);
	void stream_open(const char *filename, AVInputFormat *iformat);
	void stream_cycle_channel(int codec_type);
	void toggle_full_screen();
	void toggle_audio_display();
	void refresh_loop_wait_event(int event);
	void seek_chapter(int incr);
	void event_loop();
	void do_seek(double incr);

	// get textures
	Ref<ImageTexture> get_vis_texture();
	Ref<ImageTexture> get_sub_texture();
	Ref<ImageTexture> get_vid_texture();

	// godot integration (abstract methods of VideoStreamPlayback)
	void load( String abs_path );
	void stop();
	void play();
	bool is_playing() const;
	void set_paused(bool p_paused);
	bool is_paused() const;
	void set_loop(bool p_enable);
	bool has_loop() const;
	float get_length() const;
	float get_playback_position() const;
	void seek(float p_time);
	Ref<Texture> get_texture() const;

	void set_audio_track(int p_idx);
	void set_mix_callback(AudioMixCallback p_callback, void *p_userdata);
	int get_channels() const;
	int get_mix_rate() const;

	void update(float p_delta);

	FastForwardPlayback();
	virtual ~FastForwardPlayback();
};

#endif /* MODULES_FFMPEG_FAST_FORWARD_PLAYBACK_H_ */
