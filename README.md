# ffmpeg 4 godot

integration of [ffmpeg](http://ffmpeg.org) as a generic videoplayer in [godot engine](http://godotengine.org)

goals:

- enable the majority of video encapsulation & codecs available today
- enable high definition videos (4k, 8k, etc)
- use software and hardware acceleration when it is available to offer top performances

this repo is an engine module, not a gdnative addon: it requires engine recompilation

active development branch is **dev**, **master** branch is used to provide the latest working version of the module

## license

FFmpeg allows to compile GPL(!) and non-free codecs. Depending on the modification you do on the ffmpeg compilation script, the end license of your build may vary a lot!

If you keep static compilation and do not change ffmpeg compilation flags, your build will use a mix of MIT & LGPL code. You can distribute your games and projects without sharing the source, but you will have to follow the FFmpeg specifications described [here](https://ffmpeg.org/legal.html).

## installation (linux)

```
sudo apt-get install build-essential scons pkg-config libx11-dev libxcursor-dev libxinerama-dev libgl1-mesa-dev libglu-dev libasound2-dev libpulse-dev libudev-dev libxi-dev libxrandr-dev yasm initramfs-tools opencl-dev
git clone git@github.com:godotengine/godot.git godot
cd godot/
# checkout the 3.2 branch, master is usually far ahead
git checkout 3.2
# adding ffmpeg module
cd modules/
git clone git@gitlab.com:polymorphcool/godot-ffmpeg.git ffmpeg
cd ffmpeg
git submodule update
```

### ffmpeg compilation

if you want to enable or disable modules in ffmpeg, you'll have to recompile it

first clone ffmpeg in the module folder

```
git clone https://git.ffmpeg.org/ffmpeg.git ffmpeg
```

depending on your target OS, open the compilation script:

- *scripts/linux_x86-64_release.sh* : static release librairies for linux
- *scripts/linux_x86-64_debug.sh* : static release librairies for linux
- *scripts/windows_x86-64_release.sh* : static release librairies for windows (mingw)
- *scripts/windows_x86-64_debug.sh* : static release librairies for windows (mingw)

and adapt the configuration

oce done:

```
cd scripts/
chmod +x [compilation script].sh
./[compilation script].sh
```

if script finishes successfully, you should have a new subfolder in *lib* folder containing 

- *include* : all .h files;
- *lib* : all .a files

depending on the compilation script you used:

- *scripts/linux_x86-64_release.sh* : *lib/linux/release/x86_64*
- *scripts/linux_x86-64_debug.sh* : *lib/linux/debug/x86_64*
- *scripts/windows_x86-64_release.sh* : *lib/win/release/x86_64*
- *scripts/windows_x86-64_debug.sh* : *lib/win/debug/x86_64*

windows release is built using cross-compilation, refer to [Cross-compiling for Windows](https://docs.godotengine.org/en/stable/development/compiling/compiling_for_windows.html#cross-compiling-for-windows-from-other-operating-systems) to setup your environment

make sure you are using the exact same compiler to build ffmpeg as the one you use to build the windows release

adapt `--cross-prefix=${ARCH}-w64-mingw32-` in windows scripts to match your compiler

## godot recompilation

now time to compile godot!

back to root

```
cd ../../../
scons -j8 platform=x11
```

resources:

- http://mingw-w64.org/doku.php
- https://www.msys2.org/
- https://trac.ffmpeg.org/wiki/CompilationGuide/MinGW

