/*
 * resource_loader_video_generic.h
 *
 *  Created on: Oct 6, 2020
 *      Author: frankiezafe
 */

#ifndef MODULES_FFMPEG_FAST_FORWARD_LOADER_H_
#define MODULES_FFMPEG_FAST_FORWARD_LOADER_H_

// godot includes
#include "core/object.h"
#include "core/io/resource_loader.h"
#include "core/os/file_access.h"
#include "core/os/semaphore.h"
#include "core/os/thread.h"
#include "core/ring_buffer.h"
#include "scene/resources/video_stream.h"
#include "servers/audio_server.h"

// module includes
#include "fast_forward_stream.h"

class FastForwardLoader: public ResourceFormatLoader {
public:
	virtual RES load(const String &p_path, const String &p_original_path = "",
			Error *r_error = NULL);
	virtual void get_recognized_extensions(List<String> *p_extensions) const;
	virtual bool handles_type(const String &p_type) const;
	virtual String get_resource_type(const String &p_path) const;
};

#endif /* MODULES_FFMPEG_FAST_FORWARD_LOADER_H_ */
